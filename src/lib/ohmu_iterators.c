// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "ohmu_iterators.h"

#include "ohmu.h"
#include "ohmu_quadrant.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_mesh.h>
#else
#include <p8est_bits.h>
#include <p8est_mesh.h>
#endif

void
ohmu_iterator_quadrant_print (p4est_iter_volume_info_t *info, void *user_data)
{
  OHMU_UNUSED (user_data);

  ohmu_quadrant_print (SC_LP_INFO, info->quad);
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
