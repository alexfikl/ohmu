// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef OHMU_PLUGINS_H
#define OHMU_PLUGINS_H

#include <ohmu_base.h>

#ifndef P4_TO_P8
#include <p4est_extended.h>
#else
#include <p8est_extended.h>
#endif

#define OHMU_REGISTER_PLUGIN(p)                                               \
  do                                                                          \
  {                                                                           \
    p->version.major = OHMU_VERSION_MAJOR;                                    \
    p->version.minor = OHMU_VERSION_MINOR;                                    \
  } while (0)

/* forward declaration to avoid dependency cycle */
typedef struct ohmu ohmu_t;

/**
 * \brief Type of the plugin initializer function.
 *
 * This function initializes the plugin. There are two things that each plugin
 * has to do in the initializer function:
 *  * set the data_size of the plugin-defined cell contents of each patch.
 *  This is simply:
 *          plugin->data_size = sizeof (plugin_data_t)
 *  * call OHMU_REGISTER_PLUGIN to set the library version with which the
 *  plugin was compiled with. This is important because the interface might
 *  change between versions. This is done using:
 *          OHMU_REGISTER_PLUGIN (plugin)
 *
 * Besides this, the user can initialize any other variables. If the plugin
 * need other long lasting information to be stored, it can use the user_pointer.
 * Plugin-specific information from the configuration file can also be read
 * using the main configuration reader.
 *
 * Status: Has to be implemented.
 */
typedef void (*ohmu_plugin_initialize_t) (ohmu_t *ohmu);

/**
 * \brief Type of the plugin finalizer function.
 *
 * This should free the user pointer, if it was allocated and any other data
 * structures specific to the plugin.
 *
 * Status: Has a default implementation that does nothing.
 */
typedef void (*ohmu_plugin_finalize_t) (ohmu_t *ohmu);

/**
 * \brief Type for the plugin quadrant initialization function
 *
 * Status: Has a default implementation that initializes the quadrant user
 * data to 0.
 *
 * \sa p4est_init_t
 */
typedef p4est_init_t ohmu_plugin_init_quadrant_t;

/**
 * \brief Type for a plugin quadrant replacement function.
 *
 * Status: Has to be implemented.
 *
 * \sa p4est_replace_t
 */
typedef p4est_replace_t ohmu_plugin_replace_quadrant_t;

/**
 * \brief Type for a plugin function that gets called before each time step.
 *
 * Status: Can be NULL, does not need to be implemented.
 */
typedef void (*ohmu_plugin_step_begin_t) (ohmu_t *ohmu);

/**
 * \brief Type for a plugin function that compute the CFL condition.
 *
 * Status: Has to be implemented.
 */
typedef double (*ohmu_plugin_step_cfl_t) (ohmu_t *ohmu);

/**
 * \brief Type for a plugin function that updates the values in a cell.
 *
 * This function should update the value in the given quadrant to t + dt.
 */
typedef void (*ohmu_plugin_step_advance_t) (ohmu_t *ohmu);

/**
 * \brief Type for a plugin function that gets called after each time step.
 *
 * Status: Has to be implemented.
 */
typedef void (*ohmu_plugin_step_end_t) (ohmu_t *ohmu);

/**
 * \brief Type for a plugin function that is called to mark quadrants.
 *
 * This function should iterate over all the quadrants and use the
 * p4est_wrap_mark_refine and p4est_wrap_mark_coarsen functions to mark
 * quadrants.
 *
 * Status: Has to be implemented.
 *
 * \return 0 if no quadrants have been marked, 1 otherwise.
 */
typedef int (*ohmu_plugin_step_mark_t) (ohmu_t *ohmu);

/**
 * \brief Type for a plugin function that is called to save the solution.
 *
 * This function lets the plugin save any variables it desires using the
 * ohmu_hdf5_writer_t struct and accompanying API.
 *
 * Status: Has to be implemented.
 */
typedef void (*ohmu_plugin_step_save_t) (ohmu_t *ohmu);

/* all the plugin stuff */
typedef struct
{
  void                          *user_pointer;
  void                          *ghost_data;
  size_t                         data_size;

  ohmu_version_t                 version;
  char                          *name;
  char                          *abs_path;
  void                          *handle;

  ohmu_plugin_initialize_t       initialize;
  ohmu_plugin_finalize_t         finalize;
  ohmu_plugin_init_quadrant_t    initialize_quadrant;
  ohmu_plugin_replace_quadrant_t replace_quadrant;

  ohmu_plugin_step_begin_t       step_begin;
  ohmu_plugin_step_cfl_t         step_cfl;
  ohmu_plugin_step_advance_t     step_advance;
  ohmu_plugin_step_end_t         step_end;
  ohmu_plugin_step_mark_t        step_mark;
  ohmu_plugin_step_save_t        step_save;
} ohmu_plugin_t;

/**
 * \brief Load a plugin.
 *
 * "name" is the name of the plugin to load. The name of the plugin can be:
 *  * an absolute path to a shared library.
 *  * a simple name. This name has to point to a library called
 * "libohmu_name.so" that is in the OHMU_PLUGIN_DIR.
 */
ohmu_plugin_t *ohmu_plugin_load (const char *name);

/**
 * \brief Check if the version given by the plugin matches the one of ohmu.
 *
 * If they do not match, we will unload the plugin.
 */
int ohmu_plugin_check_version (ohmu_plugin_t *plugin);

/**
 * \brief Unload a plugin.
 *
 * Unloads the loaded library.
 */
void ohmu_plugin_unload (ohmu_plugin_t *plugin);

#endif /* OHMU_PLUGINS_H */
