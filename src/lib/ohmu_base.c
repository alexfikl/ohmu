// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "ohmu_base.h"

int ohmu_package_id = -1;

void
ohmu_init (MPI_Comm mpicomm, sc_log_handler_t log_handler, sig_t sig_handler,
           int log_threshold)
{
  struct sigaction sa;
  int              sc_catch_signals = (sig_handler != NULL ? 0 : 1);

  sc_init (mpicomm, sc_catch_signals, 1, log_handler, log_threshold);
  p4est_init (log_handler, log_threshold);

  ohmu_package_id
      = sc_package_register (log_handler, log_threshold, "ohmu",
                             "A p4est-based library for fluid dynamics");

  /* setup signal handler if the libsc one isn't set */
  if (!sc_catch_signals)
  {
    sa.sa_handler = sig_handler;
    sa.sa_flags = SA_RESTART;
    sigemptyset (&sa.sa_mask);

    SC_CHECK_ABORT (sigaction (SIGUSR1, &sa, NULL) != -1,
                    "Cannot handle signal SIGUSR1");
    SC_CHECK_ABORT (sigaction (SIGUSR2, &sa, NULL) != -1,
                    "Cannot handle signal SIGUSR2");
  }

  OHMU_GLOBAL_ESSENTIALF ("This is %s %d.%d-%s\n", OHMU_PACKAGE_STRING,
                          OHMU_VERSION_MAJOR, OHMU_VERSION_MINOR,
                          OHMU_VERSION_HASH);
}

void
ohmu_finalize (void)
{
  sc_finalize ();
}

ohmu_version_t
ohmu_version (void)
{
  ohmu_version_t v
      = { .major = OHMU_VERSION_MAJOR, .minor = OHMU_VERSION_MINOR };

  return v;
}

int
ohmu_file_empty (FILE *fd)
{
  if (fd == NULL)
  {
    return 1;
  }

  fseek (fd, 0, SEEK_END);
  return (ftell (fd) == 0 ? 1 : 0);
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
