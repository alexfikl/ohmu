// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include <ohmu.h>

volatile sig_atomic_t signal_finished = 0;

/**
 * \brief Catch the SIGUSR signals to stop the simulation.
 *
 * We can only catch SIGUSR1 and SIGUSR2 because mpirun doesn't forward any
 * other signals.
 */
static void
signal_handler_sigusr (int sig)
{
  if (sig == SIGUSR1 || sig == SIGUSR2)
  {
    signal_finished = 1;
  }
}

int
main (int argc, char **argv)
{
  sc_MPI_Comm          mpicomm = sc_MPI_COMM_NULL;
  int                  mpisize = 0;
  int                  mpirank = 0;
  int                  mpiret = 0;

  int                  log_level = 0;
  char                *config_filename;

  ohmu_t              *ohmu = NULL;
  ohmu_configreader_t *cfg = NULL;

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  /* handle command-line arguments */
  if (argc == 2 && strcmp (argv[1], "--help") == 0)
  {
    if (mpirank == 0)
    {
      printf ("usage: %s [CONFIG_FILE] [LOG_LEVEL]\n", argv[0]);
    }
    goto cleanup;
  }
  else if (argc == 1)
  {
    log_level = SC_LP_PRODUCTION;
    config_filename = NULL;
  }
  else if (argc == 2)
  {
    log_level = SC_LP_PRODUCTION;
    config_filename = argv[1];
  }
  else
  {
    log_level = atoi (argv[2]);
    config_filename = argv[1];
  }

  /* init the log handler and stuff */
  ohmu_init (mpicomm, NULL, signal_handler_sigusr, log_level);

  /* init configuration reader */
  cfg = ohmu_configreader_new (config_filename);

  /* create solver: also creates all the p4est structs */
  ohmu = ohmu_new (mpicomm, cfg);

  /* perform the time loop */
  while (!ohmu_finished (ohmu) && !signal_finished)
  {
    ohmu_advance (ohmu);
    ohmu_adapt (ohmu);
    ohmu_save (ohmu);
  }

  /* destroy our data structures */
  ohmu_destroy (ohmu, signal_finished);
  ohmu_configreader_destroy (cfg);

  /* clean up and exit */
  ohmu_finalize ();

cleanup:
  return sc_MPI_Finalize ();
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
