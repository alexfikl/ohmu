PYTHON := 'python -X dev'
BUILDDIR := "build"

_default:
    @just --list

# {{{ format

[doc("Reformat all source code")]
format: justfmt mesonfmt clangfmt

[private]
clang_format directory:
    find {{ directory }} -type f -name '*.c' -exec clang-format -i {} \;
    find {{ directory }} -type f -name '*.h' -exec clang-format -i {} \;

[doc("Run clang-format over all source files")]
clangfmt:
    @just clang_format src
    @just clang_format examples

[private]
cmake_format directory:
    find {{ directory }} -type f -name CMakeLists.txt -exec gersemi -l 100 -i {} \;

[doc("Run meson format over all meson files")]
mesonfmt:
    meson format --inplace --recursive
    @echo -e "\e[1;32mmeson format clean!\e[0m"

[doc("Run just --fmt over the justfiles")]
justfmt:
    just --unstable --fmt
    @echo -e "\e[1;32mjust --fmt clean!\e[0m"

# }}}
# {{{ lint

[doc('Run all linting checks over the source code')]
lint: typos reuse

[doc('Run typos over the source code and documentation')]
typos:
    typos --sort
    @echo -e "\e[1;32mtypos clean!\e[0m"

[doc('Check REUSE license compliance')]
reuse:
    {{ PYTHON }} -m reuse lint
    @echo -e "\e[1;32mREUSE compliant!\e[0m"

# }}}
# {{{ develop

[doc("Build project in debug mode")]
build compiler="clang": purge
    CC={{ compiler }} meson setup \
        --buildtype=debug \
        {{ BUILDDIR }}
    meson compile -C {{ BUILDDIR }}

[doc("Rebuild the project")]
rebuild:
    meson compile -C {{ BUILDDIR }}

[doc("Regenerate ctags")]
ctags:
    ctags --recurse=yes \
        --tag-relative=yes \
        --exclude=.git \
        --exclude=docs

[doc("Remove various build artifacts")]
clean:
    rm -rf docs/build.sphinx

[doc("Remove all generated build files")]
purge: clean
    rm -rf {{ BUILDDIR }} tags

# }}}
