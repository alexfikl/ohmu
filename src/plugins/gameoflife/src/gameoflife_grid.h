// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef GAMEOFLIFE_GRID_H
#define GAMEOFLIFE_GRID_H

#include <ohmu.h>

typedef struct
{
  size_t   size;
  uint32_t required_level;
  size_t   xoffset;
  size_t   yoffset;

  int     *data;
} life_grid_t;

life_grid_t *life_grid_new (const char *filename, int level);

void         life_grid_destroy (life_grid_t *grid);

int          life_grid_cell (life_grid_t *grid, size_t i, size_t j);

#endif /* GAMEOFLIFE_GRID_H */
