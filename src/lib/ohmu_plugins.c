// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "ohmu.h"

#include <dlfcn.h>

#define LOAD_FUNCTION(FN, TYPE, H, SYMBOL, DEFAULT)                           \
  do                                                                          \
  {                                                                           \
    FN = (TYPE)dlsym (H, SYMBOL);                                             \
    char *_err = dlerror ();                                                  \
    if (_err != NULL)                                                         \
    {                                                                         \
      OHMU_GLOBAL_INFOF ("Warning: %s\n", _err);                              \
      OHMU_GLOBAL_INFOF ("Using default \"%s\" for symbol \"%s\"\n",          \
                         #DEFAULT, SYMBOL);                                   \
      FN = DEFAULT;                                                           \
    }                                                                         \
  } while (0)

static char *
ohmu_plugin_clean_name (const char *name)
{
  char        clean_name[BUFSIZ];
  const char *pch = NULL;
  int         i = 0;

  // find the last backslash
  pch = strrchr (name, '/');
  if (pch != NULL)
  { // if the patch contained a backslash, it is a full path
    // skip the libohmu_ part
    pch += 1 + strlen ("libohmu_");

    // copy the rest
    for (i = 0; pch[i] != '.'; ++i)
    {
      clean_name[i] = pch[i];
    }
    clean_name[i] = '\0';

    return OHMU_STRDUP (clean_name);
  }

  return OHMU_STRDUP (name);
}

static char *
ohmu_plugin_absolute_path (const char *name)
{
  char path[BUFSIZ];

  if (name[0] == '/')
  {
    snprintf (path, BUFSIZ, "%s", name);
  }
  else
  {
    snprintf (path, BUFSIZ, "%s/libohmu_%s.so", OHMU_PLUGIN_DIR, name);
  }

  return OHMU_STRDUP (path);
}

static void
ohmu_finalize_default (ohmu_t *ohmu)
{
  OHMU_UNUSED (ohmu);
}

static void
ohmu_initialize_quadrant_default (p4est_t *p4est, p4est_topidx_t which_tree,
                                  p4est_quadrant_t *quad)
{
  OHMU_UNUSED (which_tree);

  memset (quad->p.user_data, 0, p4est->data_size);
}

ohmu_plugin_t *
ohmu_plugin_load (const char *name)
{
  char           function_name[BUFSIZ];
  ohmu_plugin_t *p = OHMU_ALLOC (ohmu_plugin_t, 1);

  p->name = ohmu_plugin_clean_name (name);
  p->abs_path = ohmu_plugin_absolute_path (name);

  p->handle = dlopen (p->abs_path, RTLD_NOW);
  if (p->handle == NULL)
  {
    SC_GLOBAL_INFOF ("Error: %s\n", dlerror ());
    SC_ABORTF ("Cannot load plugin \"%s\" from %s.", p->name, p->abs_path);
  }
  else
  {
    OHMU_GLOBAL_INFO ("Successfully loaded plugin.\n");
  }

  snprintf (function_name, BUFSIZ, "%s_initialize", p->name);
  LOAD_FUNCTION (p->initialize, ohmu_plugin_initialize_t, p->handle,
                 function_name, NULL);

  snprintf (function_name, BUFSIZ, "%s_finalize", p->name);
  LOAD_FUNCTION (p->finalize, ohmu_plugin_finalize_t, p->handle, function_name,
                 ohmu_finalize_default);

  snprintf (function_name, BUFSIZ, "%s_initialize_quadrant", p->name);
  LOAD_FUNCTION (p->initialize_quadrant, ohmu_plugin_init_quadrant_t,
                 p->handle, function_name, ohmu_initialize_quadrant_default);

  snprintf (function_name, BUFSIZ, "%s_replace_quadrant", p->name);
  LOAD_FUNCTION (p->replace_quadrant, ohmu_plugin_replace_quadrant_t,
                 p->handle, function_name, NULL);

  snprintf (function_name, BUFSIZ, "%s_step_begin", p->name);
  LOAD_FUNCTION (p->step_begin, ohmu_plugin_step_begin_t, p->handle,
                 function_name, NULL);

  snprintf (function_name, BUFSIZ, "%s_step_cfl", p->name);
  LOAD_FUNCTION (p->step_cfl, ohmu_plugin_step_cfl_t, p->handle, function_name,
                 NULL);

  snprintf (function_name, BUFSIZ, "%s_step_advance", p->name);
  LOAD_FUNCTION (p->step_advance, ohmu_plugin_step_advance_t, p->handle,
                 function_name, NULL);

  snprintf (function_name, BUFSIZ, "%s_step_end", p->name);
  LOAD_FUNCTION (p->step_end, ohmu_plugin_step_end_t, p->handle, function_name,
                 NULL);

  snprintf (function_name, BUFSIZ, "%s_step_mark", p->name);
  LOAD_FUNCTION (p->step_mark, ohmu_plugin_step_mark_t, p->handle,
                 function_name, NULL);

  snprintf (function_name, BUFSIZ, "%s_step_save", p->name);
  LOAD_FUNCTION (p->step_save, ohmu_plugin_step_save_t, p->handle,
                 function_name, NULL);

  return p;
}

int
ohmu_plugin_check_version (ohmu_plugin_t *plugin)
{
  ohmu_version_t v = ohmu_version ();
  ohmu_version_t pv = plugin->version;

  OHMU_GLOBAL_INFOF ("plugin version: %d.%d\n", pv.major, pv.minor);
  return pv.major != v.major || pv.minor != v.minor;
}

void
ohmu_plugin_unload (ohmu_plugin_t *plugin)
{
  dlclose (plugin->handle);
  OHMU_FREE (plugin->name);
  OHMU_FREE (plugin->abs_path);

  OHMU_FREE (plugin);
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
