// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "ohmu_parameters.h"

#define OHMU_LIMITS(x, a, b) (((x) >= (a)) && ((x) <= (b)))

static int
ohmu_parameters_check_time_scheme (ohmu_time_scheme_t scheme)
{
  switch (scheme)
  {
  case OHMU_TIME_FORWARD_EULER:
  case OHMU_TIME_GODUNOV_SPLITTING:
  case OHMU_TIME_STRANG_SPLITTING:
  case OHMU_TIME_SUBCYCLE:
    return 1;
  default:
    return 0;
  }

  return 0;
}

static void
ohmu_parameters_check (ohmu_parameters_t *params)
{
  SC_CHECK_ABORT (ohmu_parameters_check_time_scheme (params->time_scheme),
                  "Requested time scheme is not supported.");
  SC_CHECK_ABORT (params->tmax > 0,
                  "The final time has to be strictly bigger than 0.");
  SC_CHECK_ABORT (params->cfl > 0, "The CFL coefficient has to be positive.");
  SC_CHECK_ABORT (OHMU_FUZZY_LIMITS (params->epsilon_refine, 0, 1),
                  "Refining threshold has to be between [0, 1].");
  SC_CHECK_ABORT (OHMU_FUZZY_LIMITS (params->epsilon_coarsen, 0, 1),
                  "Coarsening threshold has to be between [0, 1].");

  SC_CHECK_ABORT (params->plugin_name != NULL,
                  "Must specify a plugin using the plugin_name key.");

  SC_CHECK_ABORT (OHMU_LIMITS (params->min_allowed_level, 0, P4EST_MAXLEVEL),
                  "The minimum refinement level has to be in [0, 30].");
  SC_CHECK_ABORT (OHMU_LIMITS (params->max_allowed_level, 0, P4EST_MAXLEVEL),
                  "The maximum refinement level has to be in [0, 30].");
  SC_CHECK_ABORT (
      params->max_allowed_level >= params->min_allowed_level,
      "The maximum refinement level has to be bigger than the min.");
  SC_CHECK_ABORT (params->connectivity_name != NULL,
                  "Must specify a connectivity.");

  SC_CHECK_ABORT (params->restart_file != NULL,
                  "Must specify a filename for the initial condition.");
  SC_CHECK_ABORT (OHMU_FUZZY_LIMITS (params->mesh_scale, 0, 1),
                  "The scale is a value between [0, 1].");
}

void
ohmu_parameters_init (ohmu_parameters_t *params, ohmu_configreader_t *cfg)
{
  /* TODO: read from config file */
  params->time_scheme = OHMU_TIME_FORWARD_EULER;
  params->tmax = ohmu_config_read_double (cfg, "iteration.tmax", 1.0);
  params->imax = ohmu_config_read_int (cfg, "iteration.imax", -1);
  params->cfl = ohmu_config_read_double (cfg, "iteration.cfl", 1.0);
  params->epsilon_refine
      = ohmu_config_read_double (cfg, "iteration.epsilon_refine", 0.1);
  params->epsilon_coarsen
      = ohmu_config_read_double (cfg, "iteration.epsilon_coarsen", 0.1);

  params->plugin_name = ohmu_config_read_string (cfg, "plugin.name", NULL);

  params->min_allowed_level
      = ohmu_config_read_int (cfg, "forest.min_allowed_level", 5);
  params->max_allowed_level
      = ohmu_config_read_int (cfg, "forest.max_allowed_level", 7);
  params->connectivity_name
      = ohmu_config_read_string (cfg, "forest.connectivity", NULL);

  params->restart_file
      = ohmu_config_read_string (cfg, "io.restart_file", NULL);
  params->mesh_scale = ohmu_config_read_double (cfg, "io.mesh_scale", 1.0);
  params->mesh_level = ohmu_config_read_int (cfg, "io.mesh_level", 0);
  params->mesh_treeid = ohmu_config_read_int (cfg, "io.mesh_treeid", 0);
  params->mesh_rank = ohmu_config_read_int (cfg, "io.mesh_rank", 0);
  params->outputs = ohmu_config_read_double (cfg, "io.outputs", 100);

  ohmu_parameters_check (params);
}
