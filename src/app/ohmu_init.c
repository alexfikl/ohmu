// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include <stdint.h>

#ifdef P4_TO_P8
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_connectivity.h>
#include <p4est_extended.h>
#else
#include <p8est_connectivity.h>
#include <p8est_extended.h>
#endif

#include <ohmu_configreader.h>

/**
 * \brief Struct describing the cell data for the transport equation.
 *
 * We're also storing the velocity per-cell, even though it is usually only
 * required per-face.
 */
typedef struct
{
  double c;                   /**< Transported quantity (cell average) */
  double velocity[P4EST_DIM]; /**< Velocity field (2D or 3D) */
} transport_data_t;

/**
 * \brief General struct describing the problem.
 */
typedef struct
{
  ohmu_configreader_t *cfg; /**< Configuration reader. */

  const char          *filename;       /**< Filename for the output file. */
  int                  save_partition; /**< See p4est_save_ext. */
  int                  min_level;      /**< Min level in the forest. */
  int                  max_level;      /**< Max level in the forest. */
  double               epsilon_refine; /**< Refinement threshold in [0, 1]. */
} context_t;

/**
 * \brief Create a new context structure.
 *
 * \param[in] filename Name of the Lua file.
 */
static context_t *
context_new (const char *filename)
{
  context_t *ctx = P4EST_ALLOC_ZERO (context_t, 1);

  ctx->cfg = ohmu_configreader_new (filename);
  ctx->filename = ohmu_config_read_string (ctx->cfg, "output", NULL);
  ctx->save_partition = ohmu_config_read_int (ctx->cfg, "save_partition", 0);
  ctx->min_level = ohmu_config_read_int (ctx->cfg, "min_level", 5);
  ctx->max_level = ohmu_config_read_int (ctx->cfg, "max_level", 8);
  ctx->epsilon_refine
      = ohmu_config_read_double (ctx->cfg, "epsilon_refine", 0.1);

  return ctx;
}

/**
 * \brief Free the context structure and close the Lua state.
 */
static void
context_destroy (context_t *ctx)
{
  ohmu_configreader_destroy (ctx->cfg);
  P4EST_FREE (ctx);
}

/****************************************************************************
 *                            p4est
 ***************************************************************************/

static void
init_data_fn (context_t *ctx, transport_data_t *w, double x[3])
{
  double result[4];

  ohmu_config_evaluate (ctx->cfg, "f", 3, x, 4, result);

#ifdef P4_TO_P8
  w->velocity[2] = result[3];
#endif
  w->velocity[1] = result[2];
  w->velocity[0] = result[1];
  w->c = result[0];
}

/**
 * \brief Initialize the data inside a quadrant.
 *
 * Uses the function defined in the Lua configuration file to initialize the
 * values in quadrant->p.user_data.
 *
 * \sa p4est_init_t.
 */
static void
init_fn (p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *q)
{
  transport_data_t *w = (transport_data_t *)q->p.user_data;
  context_t        *ctx = (context_t *)p4est->user_pointer;

  p4est_qcoord_t    half = P4EST_QUADRANT_LEN (q->level) / 2;
  double            xyz[3] = { 0, 0, 0 };

  /* compute the coordinates of the center */
  p4est_qcoord_to_vertex (p4est->connectivity, which_tree, q->x + half,
                          q->y + half,
#ifdef P4_TO_P8
                          q->z + half,
#endif
                          xyz);

  init_data_fn (ctx, w, xyz);
}

/**
 * \brief Refinement callback.
 *
 * \sa p4est_refine_t.
 */
static int
refine_fn (p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *q)
{
  double               eps = 0;
  double               grad = 0;
  context_t           *ctx = (context_t *)p4est->user_pointer;

  transport_data_t     wl;
  transport_data_t     w = *((transport_data_t *)q->p.user_data);
  transport_data_t     wr;

  double               xl[3] = { 0, 0, 0 };
  double               xr[3] = { 0, 0, 0 };

  const p4est_qcoord_t h = P4EST_QUADRANT_LEN (q->level);
  const p4est_qcoord_t half = h / 2;
  const p4est_qcoord_t offset[18]
      = { 0,    half, half, h,    half, half, half, 0,    half,
          half, h,    half, half, half, 0,    half, half, h };

  /* compute the L^2 norm of the gradient in the quadrant */
  for (int i = 0; i < P4EST_DIM; ++i)
  {
    p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                            q->x + offset[6 * i + 0], q->y + offset[6 * i + 1],
#ifdef P4_TO_P8
                            q->z + offset[6 * i + 2],
#endif
                            xl);

    p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                            q->x + offset[6 * i + 3], q->y + offset[6 * i + 4],
#ifdef P4_TO_P8
                            q->z + offset[6 * i + 5],
#endif
                            xr);

    init_data_fn (ctx, &wl, xl);
    init_data_fn (ctx, &wr, xr);

    grad = (wr.c + wl.c - 2 * w.c) / (h * h);
    eps += grad * grad;
  }

  return sqrt (eps) > ctx->epsilon_refine;
}

/****************************************************************************
 *                            Main
 ***************************************************************************/
static void
usage (char *prog)
{
  if (!sc_is_root ())
  {
    return;
  }

  printf ("usage: %s [LUA_FILE]\n", prog);
  printf ("\nExample Lua File:\n");
  printf ("\toutput = \"init.p4est\"\n");
  printf ("\tsave_partition = 0\n");
  printf ("\tmin_level = 5\n");
  printf ("\tmax_level = 8\n");
  printf ("\tepsilon_refine = 0.1\n");
  printf ("\n");
  printf ("\tfunction f(x, y, z)\n");
  printf ("\t\treturn math.exp(x^2 + y^2)\n");
  printf ("\tend\n");
}

int
main (int argc, char **argv)
{
  sc_MPI_Comm           mpicomm = sc_MPI_COMM_WORLD;
  int                   mpisize = 0;
  int                   mpirank = 0;
  int                   mpiret = 0;

  context_t            *context = NULL;
  p4est_t              *p4est = NULL;
  p4est_connectivity_t *connectivity = NULL;

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  if (argc == 1 && mpirank == 0)
  {
    usage (argv[0]);
    sc_MPI_Finalize ();
    exit (1);
  }

  sc_init (mpicomm, 1, 1, NULL, SC_LP_ALWAYS);
  p4est_init (NULL, SC_LP_ALWAYS);

  context = context_new (argv[1]);
  connectivity = p4est_connectivity_new_unitsquare ();
  p4est = p4est_new_ext (mpicomm,                   /* global communicator */
                         connectivity,              /* connectivity */
                         0,                         /* min quadrants */
                         context->min_level,        /* min level */
                         1,                         /* uniform fill */
                         sizeof (transport_data_t), /* size of user_data */
                         init_fn,                   /* init function */
                         context);                  /* user_pointer */

  /* refine */
  p4est_refine_ext (p4est, 1, context->max_level, refine_fn, init_fn, NULL);

  /* save the resulting forest */
  p4est_save_ext (context->filename, p4est, 1, context->save_partition);

  /* destroy the p4est and its connectivity structure */
  p4est_destroy (p4est);
  p4est_connectivity_destroy (connectivity);
  context_destroy (context);

  /* clean up and exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
