// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include <p4est_bits.h>
#include <p4est_connectivity.h>
#include <p4est_extended.h>
#include <p4est_ghost.h>
#include <p4est_iterate.h>
#include <p4est_vtk.h>

#define UNUSED(x) ((void)x)

static int counter;

void
quadrant_pprint (p4est_quadrant_t *q, int is_ghost, int rank)
{
  if (q == NULL)
  {
    printf ("%s quad missing\n", is_ghost ? "ghost" : "");
    SC_ABORT ("wee\n");
    return;
  }

  p4est_qcoord_t x = (q->x) >> (P4EST_QMAXLEVEL - q->level);
  p4est_qcoord_t y = (q->y) >> (P4EST_QMAXLEVEL - q->level);

  printf ("[p4est %d] x %d y %d level %d", rank, x, y, q->level);
  if (is_ghost)
  {
    printf (" value ghost");
  }
  else
  {
    printf (" value %d", q->p.user_int);
  }
  printf ("\n");
}

static void
init_fn (p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  quadrant->p.user_int = counter;
  counter += 1;
}

static int
refine_fn (p4est_t *p4est, p4est_topidx_t which_tree,
           p4est_quadrant_t *quadrant)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  return quadrant->y == 0 && quadrant->x == 0 && (quadrant->level < 4);
}

static void
iter_face_fn (p4est_iter_face_info_t *info, void *user_data)
{
  UNUSED (user_data);

  int                     mpirank = info->p4est->mpirank;
  p4est_quadrant_t       *quad = NULL;
  p4est_iter_face_side_t *side = NULL;

  for (size_t i = 0; i < info->sides.elem_count; ++i)
  {
    side = p4est_iter_fside_array_index (&info->sides, i);

    if (!side->is_hanging)
    {
      quad = side->is.full.quad;

      quadrant_pprint (quad, side->is.full.is_ghost, mpirank);
    }
    else
    {
      for (int j = 0; j < P4EST_HALF; ++j)
      {
        quad = side->is.hanging.quad[j];
        quadrant_pprint (quad, side->is.hanging.is_ghost[j], mpirank);
      }
    }
  }
}

int
main (int argc, char **argv)
{
  sc_MPI_Comm           mpicomm = sc_MPI_COMM_WORLD;
  int                   mpisize = 0;
  int                   mpirank = 0;
  int                   mpiret = 0;

  p4est_t              *p4est = NULL;
  p4est_connectivity_t *connectivity = NULL;
  p4est_ghost_t        *ghost_layer = NULL;

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  /* initialize the loggers */
  sc_init (mpicomm, 1, 1, NULL, SC_LP_ALWAYS);
  p4est_init (NULL, SC_LP_ALWAYS);

  /* initialize the forest */
  connectivity = p4est_connectivity_new_unitsquare ();
  p4est = p4est_new_ext (mpicomm, connectivity, 4, 2, 1, 0, init_fn, NULL);

  // refine a few times
  p4est_refine (p4est, 1, refine_fn, init_fn);
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);
  p4est_vtk_write_file (p4est, NULL, "ghost3d");

  // iterate and find a missing ghost
  ghost_layer = p4est_ghost_new (p4est, P4EST_CONNECT_FACE);
  p4est_iterate (p4est, ghost_layer, NULL, NULL, iter_face_fn, NULL);

  /* destroy p4est and its connectivity structure */
  p4est_destroy (p4est);
  p4est_connectivity_destroy (connectivity);
  p4est_ghost_destroy (ghost_layer);

  /* clean up and exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
