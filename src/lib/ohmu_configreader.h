// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef OHMU_CONFIGREADER_H
#define OHMU_CONFIGREADER_H

#include <lua.h>
#include <sc_containers.h>

/**
 * \brief A configuration reader that uses Lua files.
 *
 * The configuration file can contain variables that define a configuration
 * option. For example, if we set up the Lua file to contain:
 *
 *  settings = {
 *       name = "Leroy Jenkins",
 *       age = 66
 *  }
 *
 * we can then read those values by calling:
 *      name = config_read_string(cfg, "settings.name", NULL);
 *      age = config_read_int(cfg, "settings.age", 42);
 *
 * There is no need to allocate or deallocate the string as this is taken
 * care of by the configreader.
 */
typedef struct
{
  lua_State *L;       /** The Lua state */
  sc_list_t *strings; /** String values */
} ohmu_configreader_t;

/**
 * \brief Create a new configuration reader.
 *
 * The filename for the configuration file can be NULL. In this case, when
 * calling a read_* function, it automatically returns the default preset.
 *
 * \param [in] filename     The configuration file, can be NULL.
 *
 * \return An initialized ohmu_configreader_t.
 */
ohmu_configreader_t *ohmu_configreader_new (const char *filename);

/**
 * \brief Free the configuration reader.
 *
 * This also frees all the strings in the string list, so make sure that they
 * are not used after destroying the reader.
 *
 * \param [out] cfg     The configuration reader to be freed.
 */
void ohmu_configreader_destroy (ohmu_configreader_t *cfg);

/**
 * \brief Read an integer from the configuration file.
 *
 * \param [in] cfg      The configuration reader.
 * \param [in] key      The name of the field.
 * \param [out] preset  The default to use in case the key does not exist.
 *
 * \return The read value or the default preset, if the key does not exist.
 */
int ohmu_config_read_int (ohmu_configreader_t *cfg, const char *key,
                          int preset);

/**
 * \brief Read a double from the configuration file.
 *
 * \param [in] cfg      The configuration reader.
 * \param [in] key      The name of the field.
 * \param [out] preset  The default to use in case the key does not exist.
 *
 * \return The read value or the default preset, if the key does not exist.
 */
double ohmu_config_read_double (ohmu_configreader_t *cfg, const char *key,
                                double preset);

/**
 * \brief Read a string from the configuration file.
 *
 * \param [in] cfg      The configuration reader.
 * \param [in] key      The name of the field.
 *
 * \return The string corresponding to the key or the default preset, if
 * it doesn't exist.
 */
const char *ohmu_config_read_string (ohmu_configreader_t *cfg, const char *key,
                                     const char *preset);

/**
 * \brief Evaluate a function from the configuration file.
 *
 * \param [in] cfg      The configuration reader.
 * \param [in] funcname Name of the function to evaluate.
 * \param [in] ninps    Number of inputs to the function.
 * \param [in] x        Value of the inputs.
 * \param [in] nouts    Number of outputs of the function.
 * \param [out] fx      Function evaluations at the given points *x*. Only the
 *                      first *nouts* elements are touched.
 */
void ohmu_config_evaluate (ohmu_configreader_t *cfg, const char *funcname,
                           size_t ninps, double *x, size_t nouts, double *fx);

#endif /* !OHMU_CONFIGREADER_H */
