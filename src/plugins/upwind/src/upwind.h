// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef UPWIND_H
#define UPWIND_H

#include <ohmu.h>
#include <ohmu_solver.h>

/* main data structure */
typedef struct
{
  double c;
  double cnext;

  double velocity[P4EST_DIM];
} upwind_data_t;

/* interface functions */
/* see ohmu_plugins.h for definitions and explanations */
/**
 * \brief Initialize plugin.
 */
void upwind_initialize (ohmu_solver_t *solver);

/**
 * \brief Finalize plugin.
 */
void   upwind_finalize (ohmu_solver_t *solver);

void   upwind_initialize_quadrant (p4est_t *p4est, p4est_locidx_t treeid,
                                   p4est_quadrant_t *q);

void   upwind_begin_step (ohmu_solver_t *solver, p4est_quadrant_t *q);

double upwind_cfl (ohmu_solver_t *solver, p4est_quadrant_t *q);

void   upwind_exchange_ghosts (ohmu_solver_t *solver, p4est_quadrant_t *q,
                               p4est_topidx_t treeid, p4est_locidx_t quadid);

void   upwind_update (ohmu_solver_t *solver, p4est_quadrant_t *quad,
                      p4est_topidx_t treeid, p4est_locidx_t quadid, double dt);

void   upwind_end_step (ohmu_solver_t *solver, p4est_quadrant_t *q);

double upwind_refine_indicator (ohmu_solver_t *solver, p4est_quadrant_t *q);

void   upwind_replace_refined (p4est_t *p4est, p4est_topidx_t treeid,
                               p4est_quadrant_t *quadout,
                               p4est_quadrant_t *quadin[P4EST_CHILDREN]);

void   upwind_replace_coarsened (p4est_t *p4est, p4est_topidx_t treeid,
                                 p4est_quadrant_t *quadout[P4EST_CHILDREN],
                                 p4est_quadrant_t *quadin);

void   upwind_save (ohmu_solver_t *solver, const char *filename);

#endif /* UPWIND_H */
