// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef OHMU_QUADRANT_H
#define OHMU_QUADRANT_H

#include <ohmu_base.h>

#ifndef P4_TO_P8
#include <p4est_iterate.h>
#include <p4est_wrap.h>
#else
#include <p8est_iterate.h>
#include <p8est_wrap.h>
#endif

/**
 * \brief A variant of the print function offered by p4est.
 *
 * Unlike the p4est_quadrant_print() function, we write the (x, y, z)
 * coordinates not shifted to the left. This allows for easier identification
 * of the quarant in the mesh because in this form, (x, y, z) are the
 * coordinates of the quadrant as if the mesh was uniformly refined
 * at the level of the given quadrant.
 *
 * \param q The quadrant.
 */
void ohmu_quadrant_print (int log_priority, p4est_quadrant_t *q);

/**
 * \brief Compute the real length of a quadrant.
 *
 * This function will compute dx (= dy).
 *
 * \param [in] quad A quadrant.
 * \return The length (height and width) of the quadrant.
 */
double ohmu_quadrant_length (p4est_quadrant_t *q);

/**
 * \brief Compute the coordinates of the center of a quadrant.
 *
 * Uses the p4est_qcoord_to_vertex() function to compute the coordinates
 * of the lower left corner and the quadrant_length() function to compute
 * the length of the quadrant. The center is then:
 *          x_c = x_0 + h / 2
 *
 * \param [in]  connectivity The connectivity of the tree.
 * \param [in]  treeid       The tree id of the tree quad belongs to.
 * \param [in]  quad         A quadrant.
 * \param [out] xyz          The coordinates of the center.
 */
void ohmu_quadrant_center_vertex (p4est_connectivity_t *connectivity,
                                  p4est_topidx_t treeid, p4est_quadrant_t *q,
                                  double vxyz[3]);

/**
 * \brief Get the level of a side.
 *
 * A side can contain either a full quadrant or two hanging quadrants of
 * the same level.
 *
 * \param [in] side The side.
 * \return The level on that side.
 */
int8_t ohmu_face_side_level (p4est_iter_face_side_t *side);

/**
 * \brief Get the one of the quadrants that is embedded inside a side.
 *
 * Depending on the type of the side (hanging or full) we can have one
 * or two quadrants in the two different structs. See p4est_iter_face_side_t.
 *
 * The returned quadrant has pad8 set to 1 or 0 depending on whether it is
 * a ghost quadrant or not. This can be verified with ohmu_quadrant_is_ghost.
 *
 * \param [in] side The side we want to extract the quadrant from.
 * \param [in] idx The index of the quadrant inside the side.
 * \param [in,out] quadid The position of the returned quadrant in the local
 * quadrants list, or the position in the ghost array, if it is a ghost
 * quadrant. This information is extracted from p4est_iter_face_side_t.
 */
p4est_quadrant_t *ohmu_face_side_quadrant (p4est_iter_face_side_t *side,
                                           int8_t idx, p4est_locidx_t *quadid);

/**
 * \brief Get a quadrant's face neighbor using the p4est_mesh_t struct.
 *
 * The returned face side contains all the fields set to the correct values,
 * as one would expect.
 *
 * \param [in] treeid The id of the tree to which the quadrant belongs to.
 * \param [in] quadid The id of the quadrant inside the given tree's array.
 * \param [in] face The face across which to look for the neighbor.
 */
p4est_iter_face_side_t ohmu_quadrant_face_neighbor (p4est_wrap_t  *pp,
                                                    p4est_topidx_t treeid,
                                                    p4est_locidx_t quadid,
                                                    int            face);

#endif /* !OHMU_QUADRANT_H */
