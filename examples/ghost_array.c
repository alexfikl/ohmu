// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include <p4est_bits.h>
#include <p4est_communication.h>
#include <p4est_connectivity.h>
#include <p4est_extended.h>
#include <p4est_ghost.h>
#include <p4est_iterate.h>
#include <p4est_vtk.h>

#define UNUSED(x) ((void)x)
#define PATCH_SIZE 2

typedef struct
{
  int             flag;

  p4est_qcoord_t *v;
} user_data_t;

int s_patch_cells;
int s_patch_data_size;

void
quadrant_pprint (p4est_quadrant_t *q, int is_ghost, int rank)
{
  if (q == NULL)
  {
    printf ("%s quad missing\n", is_ghost ? "ghost" : "");
    SC_ABORT ("wee\n");
    return;
  }

  p4est_qcoord_t x = (q->x) >> (P4EST_QMAXLEVEL - q->level);
  p4est_qcoord_t y = (q->y) >> (P4EST_QMAXLEVEL - q->level);

  printf ("[p4est %d] x %d y %d level %d", rank, x, y, q->level);
  if (is_ghost)
  {
    printf (" value ghost");
  }
  else
  {
    printf (" value %d", q->p.user_int);
  }
  printf ("\n");
}

static void
init_fn (p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  user_data_t *ud = (user_data_t *)quadrant->p.user_data;

  ud->v = P4EST_ALLOC (p4est_qcoord_t, s_patch_cells);
  ud->v[0] = quadrant->x >> (P4EST_MAXLEVEL - quadrant->level);
  ud->v[1] = quadrant->y >> (P4EST_MAXLEVEL - quadrant->level);
  ud->v[2] = 0;
  ud->v[3] = (p4est_qcoord_t)quadrant->level;
}

static void
replace_fn (p4est_t *p4est, p4est_topidx_t which_tree, int num_outgoing,
            p4est_quadrant_t *quadout[], int num_incoming,
            p4est_quadrant_t *quadin[])
{
  user_data_t *ud;

  P4EST_INFO ("replacing\n");

  if (num_outgoing == 1)
  { // we're refining
    ud = (user_data_t *)quadout[0]->p.user_data;
    P4EST_FREE (ud->v);

    for (int i = 0; i < num_incoming; ++i)
    {
      init_fn (p4est, which_tree, quadin[i]);
    }
  }
  else
  { // we're coarsening
    init_fn (p4est, which_tree, quadin[0]);

    for (int i = 0; i < num_outgoing; ++i)
    {
      ud = (user_data_t *)quadout[i]->p.user_data;
      P4EST_FREE (ud->v);
    }
  }
}

static void
iter_fn (p4est_iter_volume_info_t *info, void *user_data)
{
  UNUSED (user_data);

  user_data_t *ud = (user_data_t *)info->quad->p.user_data;
  P4EST_FREE (ud->v);
}

static int
refine_fn (p4est_t *p4est, p4est_topidx_t which_tree,
           p4est_quadrant_t *quadrant)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  double xyz[3];
  double h = (double)P4EST_QUADRANT_LEN (quadrant->level) / P4EST_ROOT_LEN;

  p4est_qcoord_to_vertex (p4est->connectivity, which_tree, quadrant->x,
                          quadrant->y, xyz);

  // the center
  xyz[0] += h / 2;
  xyz[1] += h / 2;
  xyz[2] += h / 2;

  int in_disk = ((SC_SQR (xyz[0] - 0.5) + SC_SQR (xyz[1] - 0.5)
                  + SC_SQR (xyz[2] - 0.5))
                         < 0.01
                     ? 1
                     : 0);

  return in_disk;
}

int
main (int argc, char **argv)
{
  sc_MPI_Comm           mpicomm = sc_MPI_COMM_WORLD;
  int                   mpisize = 0;
  int                   mpirank = 0;
  int                   mpiret = 0;
  p4est_t              *p4est = NULL;
  p4est_connectivity_t *connectivity = NULL;
  p4est_ghost_t        *ghost_layer = NULL;
  p4est_quadrant_t     *g = NULL;
  p4est_quadrant_t     *q = NULL;
  p4est_tree_t         *tree = NULL;
  user_data_t          *d = NULL;
  p4est_qcoord_t       *ghost_data = NULL;
  p4est_qcoord_t       *v = NULL;
  void                **mirror_data = NULL;
  p4est_locidx_t        pcurrent = 0;
  p4est_locidx_t        pnext = 0;
  p4est_locidx_t        proc_mirrors = 0;
  p4est_locidx_t        mirror_idx = 0;
  FILE                 *fd = NULL;
  char                  filename[BUFSIZ];

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  /* initialize the loggers */
  sc_init (mpicomm, 1, 1, NULL, SC_LP_ALWAYS);
  p4est_init (NULL, SC_LP_ALWAYS);

  /* initialize the forest */
  connectivity = p4est_connectivity_new_periodic ();
  p4est = p4est_new_ext (mpicomm, connectivity, 6, 4, 1, sizeof (user_data_t),
                         init_fn, NULL);

  /* open a file per process for writing */
  snprintf (filename, BUFSIZ, "ghostarray%05d.log", mpirank);
  fd = fopen (filename, "w");

  // set patch stuff
  s_patch_cells = pow (PATCH_SIZE, P4EST_DIM);
  s_patch_data_size = sizeof (p4est_qcoord_t) * s_patch_cells;

  // refine a few times
  p4est_refine_ext (p4est, 1, 7, refine_fn, NULL, replace_fn);
  p4est_balance_ext (p4est, P4EST_CONNECT_FULL, NULL, replace_fn);
  p4est_vtk_write_file (p4est, NULL, "ghost_array");

  // get ghost layer
  ghost_layer = p4est_ghost_new (p4est, P4EST_CONNECT_FACE);

  // get ghost data
  mirror_data = P4EST_ALLOC (void *, ghost_layer->mirrors.elem_count);
  ghost_data = P4EST_ALLOC (p4est_qcoord_t,
                            s_patch_cells * ghost_layer->ghosts.elem_count);

  // print and gather mirror data mirror data
  int i = 0;
  for (int proc = 0; proc < p4est->mpisize; ++proc)
  {
    pcurrent = ghost_layer->mirror_proc_offsets[proc];
    pnext = ghost_layer->mirror_proc_offsets[proc + 1];
    proc_mirrors = pnext - pcurrent;

    for (int pg = 0; pg < proc_mirrors; ++pg, ++i)
    {
      mirror_idx = ghost_layer->mirror_proc_mirrors[pcurrent + pg];

      g = p4est_quadrant_array_index (&ghost_layer->mirrors, mirror_idx);
      tree = p4est_tree_array_index (p4est->trees, g->p.piggy3.which_tree);
      q = p4est_quadrant_array_index (
          &tree->quadrants, g->p.piggy3.local_num - tree->quadrants_offset);
      d = (user_data_t *)q->p.user_data;

      fprintf (fd, "[%d > %d] %d %d %d %d\n", p4est->mpirank, proc, d->v[0],
               d->v[1], d->v[2], d->v[3]);

      mirror_data[mirror_idx] = d->v;
    }
  }

  fprintf (fd, "\n Ghosts \n");

  p4est_ghost_exchange_custom (p4est, ghost_layer, s_patch_data_size,
                               mirror_data, ghost_data);

  // print ghost data
  fprintf (fd, "%lu\n", ghost_layer->ghosts.elem_count);
  for (size_t i = 0; i < ghost_layer->ghosts.elem_count; ++i)
  {
    g = p4est_quadrant_array_index (&ghost_layer->ghosts, i);
    v = (p4est_qcoord_t *)&ghost_data[s_patch_cells * i];

    fprintf (fd, "[%d > %d] %d %d %d %d\n",
             p4est_comm_find_owner (p4est, g->p.piggy3.which_tree, g, 0),
             p4est->mpirank, v[0], v[1], v[2], v[3]);
  }

  fflush (fd);
  fclose (fd);

  // free all the quad data
  p4est_iterate (p4est, NULL, NULL, iter_fn, NULL, NULL);

  /* destroy p4est and its connectivity structure */
  p4est_destroy (p4est);
  p4est_connectivity_destroy (connectivity);
  p4est_ghost_destroy (ghost_layer);
  P4EST_FREE (mirror_data);
  P4EST_FREE (ghost_data);

  /* clean up and exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
