// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "gameoflife_grid.h"
#include "gameoflife.h"

/**
 * \brief Read a block that starts at position (x, y).
 *
 * The block that we're going to read starts at (x, y) and can expand by
 * at most 80 positions in the y direction and until we hit another
 * '#P' or the EOF in the x direction.
 *
 * The allowed characters in a block are '.' to indicate a dead cell and '*'
 * to indicate a live cell.
 */
static void
life_grid_read_line (life_grid_t *grid, const char *line, int x, int y)
{
  y = grid->size - y;

  for (int i = 0; line[i] != '\0'; ++i)
  {
    switch (line[i])
    {
    case '.': // dead cell
      // cells are already initialized to 0
      break;
    case '*': // living cell
      grid->data[grid->size * (x + i) + y] = 1;
      break;
    case '\n': // next row
    case '\r':
    case EOF:
      break;
    default: // this shouldn't happen
      SC_ABORT ("Invalid character.\n");
    }
  }
}

/**
 * \brief Read a grid from a lif file.
 *
 * Lif files are organized as follows :
 *      #Life <version>
 *      #D Comments
 *      #P x1 y1
 *      <block>
 *      #P x2 y2
 *      ...
 *
 * We read all the blocks that give the configuration of the live cells in
 * that area and put them in the right place in the global grid. The (x1, y1)
 * offsets in the lif file presume that the grid is centered in (0, 0), so
 * we also update that to center the grid in (n / 2, n / 2) where n is the
 * size of the global grid (2^7 or 2^8).
 *
 * Furthermore, the (x, y) denotes the upper left corner of the block to
 * follow.
 *
 * \param [in] fd An open file descriptor for the lif file.
 * \param [in] grid The grid we're going to read into.
 */
static void
life_grid_read (FILE *fd, life_grid_t *grid)
{
  char buffer[BUFSIZ];
  int  x = 0;
  int  y = 0;

  while (fgets (buffer, BUFSIZ, fd) != NULL)
  {
    // if we read '#P', we have to read the offsets
    if (buffer[0] == '#' && buffer[1] == 'P')
    {
      sscanf (buffer, "#P %d %d", &x, &y);

      x = x + grid->xoffset;
      y = y + grid->yoffset;
      continue;
    }

    // just a comment
    if (buffer[0] == '#')
    {
      continue;
    }

    // a line containing cells
    life_grid_read_line (grid, buffer, x, y);

    // go to next line
    ++y;
  }
}

life_grid_t *
life_grid_new (const char *filename, int level)
{
  life_grid_t *grid = OHMU_ALLOC (life_grid_t, 1);
  FILE        *fd = NULL;

  fd = fopen (filename, "r");
  SC_CHECK_ABORT (fd != NULL, "Cannot open file.\n");

  grid->size = pow (2, level);
  grid->xoffset = grid->size / 2;
  grid->yoffset = grid->size / 2;
  grid->data = OHMU_ALLOC_ZERO (int, grid->size * grid->size);

  life_grid_read (fd, grid);
  fclose (fd);

  return grid;
}

void
life_grid_destroy (life_grid_t *grid)
{
  OHMU_FREE (grid->data);
  OHMU_FREE (grid);
}

int
life_grid_cell (life_grid_t *grid, size_t i, size_t j)
{
  return grid->data[grid->size * i + j];
}
