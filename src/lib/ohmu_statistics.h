// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef OHMU_STATISTICS_H
#define OHMU_STATISTICS_H

#include <ohmu.h>

typedef struct
{
  double min;
  double max;
  double mean;
} ohmu_stat_double_t;

typedef struct
{
  p4est_gloidx_t min;
  p4est_gloidx_t max;
  double         mean;
} ohmu_stat_int_t;

typedef struct
{
  MPI_Comm           mpicomm; // the global communicator
  int                mpisize; // number of processes
  int                mpirank; // the current process

  int                iterations; // number of iterations
  double             t;          // final time

  double             norml1; // L1 norm
  double             norml2; // L2 norm

  ohmu_stat_double_t dt;
  ohmu_stat_double_t timing_scheme;
  ohmu_stat_double_t timing_io;
  ohmu_stat_double_t timing_adapt;
  ohmu_stat_int_t    level;
  ohmu_stat_int_t    num_quadrants;
} ohmu_statistics_t;

/**
 * \brief Create a new ohmu_statistics structure.
 *
 * All fields are initialized to TYPE_MAX and TYPE_MIN, as need be.
 */
ohmu_statistics_t *ohmu_statistics_new (MPI_Comm mpicomm, int mpisize,
                                        int mpirank);

/**
 * \brief Destroy the ohmu_statistics struct.
 */
void ohmu_statistics_destroy (ohmu_statistics_t *stats);

/**
 * \brief Update the min / max time step from this new value.
 */
void ohmu_statistics_dt (ohmu_statistics_t *stats, double dt);

/**
 * \brief Update the min / max levels from this new values.
 */
void ohmu_statistics_level (ohmu_statistics_t *stats, int min, int max);

/**
 * \brief Update the min / max local number of quadrants from this new value.
 */
void ohmu_statistics_num_quadrants (ohmu_statistics_t *stats, int lnq);

/**
 * \brief Update the min / max time to advance by one iteration.
 */
void ohmu_statistics_timing_scheme (ohmu_statistics_t *stats, double t);

/**
 * \brief Update the min / max time to write the solution this new value.
 */
void ohmu_statistics_timing_io (ohmu_statistics_t *stats, double t);

/**
 * \brief Update the min / max time to adapt the mesh from this new value.
 */
void ohmu_statistics_timing_adapt (ohmu_statistics_t *stats, double t);

/**
 * \brief Write the data inside the ohmu_statistics struct to a file.
 *
 * Only process 0 will write to the file. Before writing, a global reduce
 * and gather is done on all the members to get the corresponding global
 * values.
 *
 * The file is is written in append mode, so if it already exists, the new
 * data is just appended to the end. If the file does not exist, it is
 * created and an opening bracket is added.
 *
 * The rest of the file is in JSON format
 */
void ohmu_statistics_write (ohmu_statistics_t *stats, const char *filename);

#endif /* OHMU_STATISTICS_H */
