// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef UPWIND_BITS_H
#define UPWIND_BITS_H

#include "upwind.h"

#define BIT(x, k) (((x) & (1 << (k))) >> (k))

typedef struct
{
  uint32_t start;
  uint32_t end;
  int32_t  stride;
} upwind_loop_t;

upwind_data_t *upwind_quadrant_data (p4est_quadrant_t *q);

void           upwind_patch_print (upwind_data_t *patch, uint32_t patch_size,
                                   uint32_t patch_ghosts);

void           upwind_patch_cell_copy (upwind_data_t *dst, upwind_data_t *src);

void           upwind_patch_copy (upwind_data_t *dst, upwind_data_t *src,
                                  uint32_t patch_size, uint32_t patch_ghosts, int all);

void           upwind_refine_loop (uint32_t patch_size, uint32_t patch_ghosts,
                                   uint8_t child, upwind_loop_t d[P4EST_DIM]);

void           upwind_ghost_loop (uint32_t patch_size, uint32_t patch_ghosts,
                                  uint8_t face, upwind_loop_t d[3]);

upwind_data_t  upwind_patch_group_mean (upwind_data_t *patch, uint32_t size,
                                        uint32_t start);

void upwind_patch_child_copy (ohmu_solver_t *solver, upwind_data_t *child,
                              upwind_data_t *parent, uint8_t cid);

void upwind_patch_parent_copy (ohmu_solver_t *solver, upwind_data_t *child,
                               upwind_data_t *parent, uint8_t cid);

#endif /* UPWIND_BITS_H */
