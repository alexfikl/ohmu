# OHMU

This is an experimental library that uses [p4est](http://p4est.org/) to solve
hyperbolic equations. It currently implements a simple Upwind scheme for the
transport equation, with more complicated schemes to come.

## Notice

This code was significantly refactored, improved, and expanded in the
[canoP](https://gitlab.maisondelasimulation.fr/canoP/canoP) library. It's unlikely
that this version will ever get there and is best used as a prototype of
`p4est` usage.

## Naming

[Ohmu](https://en.wikipedia.org/wiki/List_of_Nausica%C3%A4_of_the_Valley_of_the_Wind_characters#Ohmu)
are creatures in the Nausicaä of the Valley of the Wind manga series (and movie).
They are protectors of the forest and are seen as a force of nature.

# Compilation

The app uses [meson](https://mesonbuild.com/) as a build system. So these simple
commands will do:
```bash
meson setup --buildtype=debug build
meson compile -C build
```

This will produce a library and executables in `build/src/app` that can be used
to launch the simulation.

# License

This code is licensed under the [CECILL-2.0](https://spdx.org/licenses/CECILL-2.0.html)
license. An appropriate SPDX header is added to all files. You can use the
[reuse command-line tool](https://github.com/fsfe/reuse-tool) to check the
license of any individual files.
