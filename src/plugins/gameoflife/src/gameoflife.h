// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef GAMEOFLIFE_H
#define GAMEOFLIFE_H

#include <ohmu.h>
#include <ohmu_solver.h>

/* main data structure */
typedef struct
{
  int c;
  int cnext;
} life_data_t;

/* interface functions */
/* see ohmu_plugins.h for definitions and explanations */
/**
 * \brief Initialize plugin.
 */
void gameoflife_initialize (ohmu_solver_t *solver);

/**
 * \brief Finalize plugin.
 */
void   gameoflife_finalize (ohmu_solver_t *solver);

void   gameoflife_initialize_quadrant (p4est_t *p4est, p4est_locidx_t treeid,
                                       p4est_quadrant_t *q);

void   gameoflife_begin_step (ohmu_solver_t *solver);

double gameoflife_cfl (ohmu_solver_t *solver);

void   gameoflife_exchange_ghosts (ohmu_solver_t *solver, p4est_quadrant_t *q,
                                   p4est_topidx_t treeid, p4est_locidx_t quadid);

void   gameoflife_update (ohmu_solver_t *solver, p4est_quadrant_t *q,
                          p4est_topidx_t treeid, p4est_locidx_t quadid,
                          double dt);

void   gameoflife_end_step (ohmu_solver_t *solver);

double gameoflife_refine_indicator (ohmu_solver_t    *solver,
                                    p4est_quadrant_t *q);

void   gameoflife_replace_refined (p4est_t *p4est, p4est_topidx_t treeid,
                                   p4est_quadrant_t *quadout,
                                   p4est_quadrant_t *quadin[P4EST_CHILDREN]);

void   gameoflife_replace_coarsened (p4est_t *p4est, p4est_topidx_t treeid,
                                     p4est_quadrant_t *quadout[P4EST_CHILDREN],
                                     p4est_quadrant_t *quadin);

int    gameoflife_should_save (ohmu_solver_t *solver);

void   gameoflife_save (ohmu_solver_t *solver, const char *filename);

#endif /* GAMEOFLIFE_H */
