// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "upwind_bits.h"

upwind_loop_t
upwind_patch_loop_t (uint32_t patch_size, uint32_t patch_ghosts)
{
  upwind_loop_t d = { .start = patch_ghosts,
                      .end = patch_size + patch_ghosts,
                      .stride = 1 };

  return d;
}

void
upwind_refine_loop (uint32_t patch_size, uint32_t patch_ghosts, uint8_t child,
                    upwind_loop_t d[P4EST_DIM])
{
  uint32_t half = patch_size / 2;

  d[0].start = patch_ghosts + BIT (child, 0) * half;
  d[0].end = patch_ghosts + (BIT (child, 0) + 1) * half;
  d[0].stride = 0;

  d[1].start = patch_ghosts + BIT (child, 1) * half;
  d[1].end = patch_ghosts + (BIT (child, 1) + 1) * half;
  d[1].stride = 0;

#ifdef P4_TO_P8
  d[2].start = patch_ghosts + BIT (child, 2) * half;
  d[2].end = patch_ghosts + (BIT (child, 2) + 1) * half;
  d[2].stride = 0;
#endif
}

void
upwind_ghost_loop (uint32_t patch_size, uint32_t patch_ghosts, uint8_t face,
                   upwind_loop_t d[3])
{
  uint8_t id = face / 2;
  int8_t  side = 2 * (face % 2 == 0) - 1;

  for (uint8_t i = 0; i < 3; ++i)
  {
    d[i].start = patch_ghosts;
    d[i].end = patch_size + patch_ghosts;
    d[i].stride = 0;
  }

  d[id].start = (face % 2 == 1) * (patch_size + patch_ghosts);
  d[id].end = (face % 2 == 1) * (patch_size + patch_ghosts) + patch_ghosts;
  d[id].stride = side * (int32_t)patch_size;
}

static inline uint32_t
upwind_child_linear_id (uint32_t pid, uint8_t cid, uint32_t size)
{
  uint32_t ssize = size * size;

  return 2 * pid + ssize * BIT (cid, 2) + size * BIT (cid, 1) + BIT (cid, 0);
}

upwind_data_t *
upwind_quadrant_data (p4est_quadrant_t *q)
{
  return (upwind_data_t *)q->p.user_data;
}

void
upwind_patch_print (upwind_data_t *patch, uint32_t nc, uint32_t ng)
{
  uint32_t n = nc + 2 * ng;
  uint32_t k = 0;
#ifdef P4_TO_P8
  const uint32_t offset = n * n * ng;
#else
  const uint32_t offset = 0;
#endif

  for (uint32_t yi = n - 1; yi < n; --yi)
  {
    for (uint32_t xi = 0; xi < n; ++xi)
    {
      k = n * yi + xi;

      if (ng <= xi && xi < (nc + ng) && ng <= yi && yi < (nc + ng))
      {
        printf ("\033[32;1m%4g\033[0m ", patch[k + offset].c);
      }
      else
      {
        printf ("%4g ", patch[k + offset].c);
      }
    }
    printf ("\n");
  }
}

void
upwind_patch_cell_copy (upwind_data_t *dst, upwind_data_t *src)
{
  dst->c = src->c;
  dst->cnext = src->cnext;

  for (uint8_t i = 0; i < P4EST_DIM; ++i)
  {
    dst->velocity[i] = src->velocity[i];
  }
}

upwind_data_t
upwind_patch_group_mean (upwind_data_t *cells, uint32_t row_size, uint32_t pid)
{
  uint32_t      gid = 0;
  upwind_data_t result;
  memset (&result, 0, sizeof (upwind_data_t));

  for (uint8_t group = 0; group < P4EST_CHILDREN; ++group)
  {
    gid = upwind_child_linear_id (pid, group, row_size);

    result.c += (cells[gid].c / P4EST_CHILDREN);

    for (uint8_t i = 0; i < P4EST_DIM; ++i)
    {
      result.velocity[i] += (cells[gid].velocity[i] / P4EST_CHILDREN);
    }
  }

  return result;
}

void
upwind_patch_child_copy (ohmu_solver_t *solver, upwind_data_t *child,
                         upwind_data_t *parent, uint8_t cid)
{
  ohmu_solver_settings_t settings = solver->settings;
  uint32_t               patch_size = settings.patch_size;
  uint32_t               patch_ghosts = settings.patch_ghosts;
  uint32_t               row_size = patch_size + 2 * patch_ghosts;
  uint32_t               slice_size = row_size * row_size;

  uint32_t               xi = 0;
  uint32_t               yi = 0;
  uint32_t               zi = 0;
  uint32_t               pid = 0;
  uint32_t               gid = 0;

  upwind_loop_t          d[P4EST_DIM];
  upwind_refine_loop (patch_size, patch_ghosts, cid, d);

  // loop over all cells in the parent's patch that overlap with the
  // current child.
#ifdef P4_TO_P8
  for (zi = d[2].start; zi < d[2].end; ++zi)
  {
#endif
    for (yi = d[1].start; yi < d[1].end; ++yi)
    {
      for (xi = d[0].start; xi < d[0].end; ++xi)
      {
        // compute the linear index of the parent cell
        pid = slice_size * zi + row_size * yi + xi;

        // loop over the 4 (or 8) "children" of the parent cell in the
        // child patch and copy the parent value into each of them
        for (uint8_t group = 0; group < P4EST_CHILDREN; ++group)
        {
          gid = upwind_child_linear_id (pid, group, row_size);

          upwind_patch_cell_copy (&child[gid], &parent[pid]);
        }
      }
    }
#ifdef P4_TO_P8
  }
#endif
}

void
upwind_patch_parent_copy (ohmu_solver_t *solver, upwind_data_t *child,
                          upwind_data_t *parent, uint8_t cid)
{
  ohmu_solver_settings_t settings = solver->settings;
  uint32_t               patch_size = settings.patch_size;
  uint32_t               patch_ghosts = settings.patch_ghosts;
  uint32_t               row_size = patch_size + 2 * patch_ghosts;
  uint32_t               slice_size = row_size * row_size;

  uint32_t               xi = 0;
  uint32_t               yi = 0;
  uint32_t               zi = 0;
  uint32_t               pid = 0;

  upwind_data_t          mean;
  upwind_loop_t          d[P4EST_DIM];
  upwind_refine_loop (patch_size, patch_ghosts, cid, d);

  // loop over all cells in parent patch that superpose on the child.
#ifdef P4_TO_P8
  for (zi = d[2].start; zi < d[2].end; ++zi)
  {
#endif
    for (yi = d[1].start; yi < d[1].end; ++yi)
    {
      for (xi = d[0].start; xi < d[0].end; ++xi)
      {
        // compute the linear index of the parent cell
        pid = slice_size * zi + row_size * yi + xi;

        // compute a mean of the child quadrants to set into the parent
        mean = upwind_patch_group_mean (child, row_size, pid);
        upwind_patch_cell_copy (&parent[pid], &mean);
      }
    }
#ifdef P4_TO_8
  }
#endif
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
