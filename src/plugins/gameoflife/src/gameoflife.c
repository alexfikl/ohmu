// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "gameoflife.h"
#include "gameoflife_grid.h"

static life_data_t *
gameoflife_quadrant_data (p4est_quadrant_t *q)
{
  return (life_data_t *)q->p.user_data;
}

/* TODO: this should be moved to libohmu somehow */
static life_data_t *
gameoflife_quadrant_corner_neighbor_data (ohmu_solver_t *solver,
                                          p4est_topidx_t treeid,
                                          p4est_locidx_t quadid, int corner)
{
  p4est_t          *p4est = ohmu_solver_p4est (solver);
  p4est_mesh_t     *mesh = ohmu_solver_mesh (solver);

  p4est_quadrant_t *q = NULL;
  p4est_tree_t     *tree = p4est_tree_array_index (p4est->trees, treeid);

  p4est_locidx_t    lnq = p4est->local_num_quadrants;
  p4est_locidx_t    qtc = 0;
  p4est_locidx_t    cornerid = 0;

  // get the id of the neighboring quadrant for the desired corner
  cornerid = P4EST_FACES * (tree->quadrants_offset + quadid) + corner;
  qtc = mesh->quad_to_corner[cornerid];

  // can be negative for tree corners
  if (qtc < 0)
  {
    return NULL;
  }

  // handle local quadrant
  if (qtc < lnq)
  {
    q = p4est_mesh_quadrant_cumulative (p4est, qtc, NULL, NULL);

    return gameoflife_quadrant_data (q);
  }

  // return ghost data
  return ohmu_ghost_data_array_index (solver, qtc - lnq);
}

void
gameoflife_initialize (ohmu_solver_t *solver)
{
#ifdef P4_TO_P8
  OHMU_ABORT ("This plugin only works in 2D");
#endif
  const char *file = NULL;
  int         level = 0;

  // read the file containing the initial solution
  file = ohmu_config_read_string (solver->cfg, "gameoflife.file", NULL);
  level = ohmu_config_read_int (solver->cfg, "gameoflife.level", 7);
  life_grid_t *grid = life_grid_new (file, level);
  solver->plugin_data = grid;

  // force the solver levels to a grid
  solver->settings.min_allowed_level = level;
  solver->settings.max_allowed_level = level;

  // force one cell per patch
  solver->settings.patch_size = 1;
  solver->settings.patch_ghosts = 0;
  solver->settings.num_patch_cells = 1;

  // mandatory !!
  solver->plugin->data_size = sizeof (life_data_t);
  OHMU_REGISTER_PLUGIN (solver->plugin);
}

void
gameoflife_finalize (ohmu_solver_t *solver)
{
  life_grid_t *grid = (life_grid_t *)solver->plugin_data;
  life_grid_destroy (grid);
}

void
gameoflife_initialize_quadrant (p4est_t *p4est, p4est_locidx_t tree,
                                p4est_quadrant_t *q)
{
  UNUSED (tree);

  ohmu_solver_t *solver = (ohmu_solver_t *)p4est->user_pointer;
  life_data_t   *data = gameoflife_quadrant_data (q);
  life_grid_t   *grid = (life_grid_t *)solver->plugin_data;

  size_t         x = ((q->x) >> (P4EST_MAXLEVEL - q->level));
  size_t         y = ((q->y) >> (P4EST_MAXLEVEL - q->level));

  data->c = data->cnext = life_grid_cell (grid, x, y);
}

void
gameoflife_begin_step (ohmu_solver_t *solver)
{
  UNUSED (solver);
}

double
gameoflife_cfl (ohmu_solver_t *solver)
{
  UNUSED (solver);

  return 0.01;
}

void
gameoflife_exchange_ghosts (ohmu_solver_t *solver, p4est_quadrant_t *q,
                            p4est_topidx_t tree, p4est_locidx_t qid)
{
  UNUSED (solver);
  UNUSED (q);
  UNUSED (tree);
  UNUSED (qid);
}

void
gameoflife_update (ohmu_solver_t *solver, p4est_quadrant_t *q,
                   p4est_topidx_t treeid, p4est_locidx_t quadid, double dt)
{
  UNUSED (dt);

  p4est_t          *p4est = ohmu_solver_p4est (solver);
  p4est_ghost_t    *ghost = ohmu_solver_ghost (solver);
  p4est_mesh_t     *mesh = ohmu_solver_mesh (solver);
  void             *ghost_data = ohmu_solver_ghost_data (solver);
  life_data_t      *data = gameoflife_quadrant_data (q);
  life_data_t      *ndata = NULL;

  p4est_quadrant_t *neighbor;
  int               neighbors = 0;
  int               nface = 0;

  // init face neighbor iterator
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, p4est, ghost, mesh, treeid, quadid);

  // loop over neighbors
  neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, &nface, NULL);
  while (neighbor != NULL)
  {
    // get the data from the face neighbor
    ndata = (life_data_t *)p4est_mesh_face_neighbor_data (&mfn, ghost_data);
    neighbors += ndata->c;

    // get the data from the corresponding corner neighbor
    ndata = gameoflife_quadrant_corner_neighbor_data (solver, treeid, quadid,
                                                      nface);
    if (ndata != NULL)
    {
      neighbors += ndata->c;
    }

    // next!
    neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, &nface, NULL);
  }

  if (data->c == 1)
  {
    data->cnext = (neighbors == 2 || neighbors == 3);
  }
  else
  {
    data->cnext = (neighbors == 3);
  }
}

static void
gameoflife_end_step_cb (p4est_iter_volume_info_t *info, void *user_data)
{
  UNUSED (user_data);

  life_data_t *data = gameoflife_quadrant_data (info->quad);
  data->c = data->cnext;
}

void
gameoflife_end_step (ohmu_solver_t *solver)
{
  p4est_t       *p4est = ohmu_solver_p4est (solver);
  p4est_ghost_t *ghost = ohmu_solver_ghost (solver);

  p4est_iterate (p4est, ghost, NULL, gameoflife_end_step_cb, NULL,
#ifdef P4_TO_P8
                 NULL,
#endif
                 NULL);
}

double
gameoflife_refine_indicator (ohmu_solver_t *solver, p4est_quadrant_t *q)
{
  UNUSED (solver);
  UNUSED (q);

  return 0;
}

void
gameoflife_replace_refined (p4est_t *p4est, p4est_topidx_t which_tree,
                            p4est_quadrant_t *quadout,
                            p4est_quadrant_t *quadin[P4EST_CHILDREN])
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (quadout);
  UNUSED (quadin);
}

void
gameoflife_replace_coarsened (p4est_t *p4est, p4est_topidx_t which_tree,
                              p4est_quadrant_t *quadout[P4EST_CHILDREN],
                              p4est_quadrant_t *quadin)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (quadout);
  UNUSED (quadin);
}

int
gameoflife_should_save (ohmu_solver_t *solver)
{
  UNUSED (solver);

  return 1;
}

void
gameoflife_save (ohmu_solver_t *solver, const char *filename)
{
  p4est_t             *p4est = ohmu_solver_p4est (solver);
  const p4est_locidx_t Ntotal = p4est->local_num_quadrants;
  p4est_locidx_t       t = 0;
  int                  k = 0;
  p4est_tree_t        *tree = NULL;
  sc_array_t          *quadrants;
  p4est_quadrant_t    *quadrant = NULL;
  life_data_t         *cell = NULL;
  int                 *cell_data = P4EST_ALLOC (int, Ntotal);

  // open the new file and write our stuff
  ohmu_hdf5_writer_open (solver->writer, filename);
  ohmu_write_header (solver->writer, solver->t);

  // fill it in
  for (t = p4est->first_local_tree; t <= p4est->last_local_tree; ++t)
  {
    tree = p4est_tree_array_index (p4est->trees, t);
    quadrants = &tree->quadrants;

    for (size_t i = 0; i < quadrants->elem_count; ++i, ++k)
    {
      quadrant = p4est_quadrant_array_index (quadrants, i);

      cell = gameoflife_quadrant_data (quadrant);
      cell_data[k] = cell->c;
    }
  }

  // write cell data
  ohmu_write_attribute (solver->writer, "cells", cell_data, 0,
                        OHMU_HDF5_CELL_SCALAR, H5T_NATIVE_INT);

  // close the file
  ohmu_write_footer (solver->writer);
  ohmu_hdf5_writer_close (solver->writer);

  P4EST_FREE (cell_data);
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
