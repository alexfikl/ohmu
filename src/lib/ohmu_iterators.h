// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef OHMU_ITERATORS_H
#define OHMU_ITERATORS_H

#include <ohmu_base.h>

#ifndef P4_TO_P8
#include <p4est_iterate.h>
#else
#include <p8est_iterate.h>
#endif

/**
 * \brief Print information about each quadrant.
 *
 * Uses the ohmu_quadrant_print function to print the (x, y, z) of the
 * quadrant as well as the level.
 */
void ohmu_iterator_quadrant_print (p4est_iter_volume_info_t *info,
                                   void                     *user_data);

#endif /* !OHMU_ITERATORS_H */
