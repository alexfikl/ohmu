// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef OHMU_HDF5_H
#define OHMU_HDF5_H

#include <ohmu_base.h>

#include <hdf5.h>
#include <hdf5_hl.h>

/**
 * \brief The four types of supported attribute types.
 */
typedef enum
{
  OHMU_HDF5_CELL_SCALAR, /**< A cell-centered scalar. */
  OHMU_HDF5_CELL_VECTOR, /**< A cell-centered vector. */
  OHMU_HDF5_NODE_SCALAR, /**< A node-centered scalar. */
  OHMU_HDF5_NODE_VECTOR  /**< A node-centered vector. */
} ohmu_hdf5_attribute_type_t;

/**
 * \brief Struct that defines a HDF5 dataset (1D or 2D).
 *
 * See H5Screate_simple and H5Sselect_hyperslab for how these are used.
 */
typedef struct
{
  hid_t rank;
  /**< Dimension of the dataset. */
  hid_t type;
  /**< Data type of the dataset. H5T_NATIVE_* */
  hsize_t dims[2];
  /**< Global size of the dataset . */
  hsize_t count[2];
  /**< Local size of the dataset. */
  hsize_t start[2];
  /**< Offset for the current process. */
} ohmu_hdf5_dataset_t;

/**
 *\brief The writer structure contains the current state of the HDF5 writer.
 *
 * When writing node data, nodes that are at the boundary between two
 * processes are written twice.
 *
 * TODO: use p4est_nodes_new with a non-NULL p4est_ghost_t to write boundary
 * nodes only once.
 */
typedef struct
{
  p4est_t *p4est; /**< Not owned */

  /* Can be modified before calling ohmu_write_header */
  double scale;       /**< Scale of each cell. */
  int    write_level; /**< Write level for each quad. */
  int    write_tree;  /**< Write treeid for each quad. */
  int    write_rank;  /**< Write mpirank for each quad. */

  /* Private information */
  char               *file_base;  /**< The basename of the files. */
  int                 file_index; /**< Number of files written. */
  char               *file_name;  /**< {file_base}_{file_index} */

  ohmu_hdf5_dataset_t quads;
  ohmu_hdf5_dataset_t nodes;

  /**
   * File descriptors for the XMF and HDF5 files we are going to write. They are
   * only valid between calls to ohmu_write_header and ohmu_write_footer.
   */
  FILE *xfd;
  hid_t hfd;
} ohmu_hdf5_t;

/**
 * \brief Create a new writer.
 *
 * The defaults are:
 *      scale           1.0
 *      write_level     1
 *      write_tree      1
 *      write_rank      1
 *
 * \param [in] name         A basename for all the files that are going to be
 *                          written. Each file will then have appended
 *                          "_XXXXXX" and the corresponding extension.
 *
 * \return A fully initialized writer with the given defaults.
 */
ohmu_hdf5_t *ohmu_hdf5_new (p4est_t *p4est, const char *name);

/**
 * \brief Extended version of the constructor.
 */
ohmu_hdf5_t *ohmu_hdf5_new_ext (p4est_t *p4est, const char *name, double scale,
                                int write_level, int write_tree,
                                int write_rank);

/**
 * \brief Free the writer struct.
 */
void ohmu_hdf5_destroy (ohmu_hdf5_t *w);

/**
 * \brief Write very basic mesh information.
 *
 * This will write the treeid, level and rank for each quadrant in the forest
 * with a scale of 0.95. It basically just calls ohmu_write_header and
 * ohmu_write_footer with a modified scale and the provided basename.
 *
 * \param [in] basename     The base name of the file to be written to.
 * \return 1 if it has written everything correctly, 0 otherwise.
 */
int ohmu_hdf5_write_mesh (p4est_t *p4est, const char *basename);

/**
 * \brief Write the header for the XMF and HDF5 files.
 *
 * The header includes the node information, connectivity information and
 * the treeid, level or mpirank for each quadrant, if requested. The extra
 * information is specified by the different members of the hdf5_writer
 * struct, so make sure to modify them before calling this function if
 * you want other values than the defaults.
 *
 * In the case of the XMF file, this will define the topology and geometry
 * of the mesh and point to the relevant fields in the HDF5 file.
 *
 * \param[in] time      The time of the current step. If this file is part of a
 *                      time series, all the times have to be different or else
 *                      it will not be correctly recognized.
 * \return 1 if it has written everything correctly, 0 otherwise.
 */
int ohmu_hdf5_write_header (ohmu_hdf5_t *w, double time);

/**
 * \brief Write a node-centered or cell-centered attribute.
 *
 * \param [in] w        The writer.
 * \param [in] name     The name of the attribute.
 * \param [in] data     The data to be written. The size of the data is computed
 *                      from the number of nodes or quadrants and the dim
 *                      argument (for vectors).
 * \param [in] dim      In the case of a vector, this is the dimension of
 *                      each element in the vector field.
 * \param [in] atype    The type of the attribute. See supported types in
 *                      the ohmu_hdf5_attribute_type_t enum.
 * \param [in] dtype    The type of the data we are writing. This is given as
 *                      a native HDF5 type. See the types defined in the
 *                      H5Tpublic.h header. Only NATIVE types are supported.
 * \return 1 if it has written everything correctly, 0 otherwise.
 */
int ohmu_hdf5_write_attribute (ohmu_hdf5_t *w, const char *name, void *data,
                               size_t dim, ohmu_hdf5_attribute_type_t atype,
                               hid_t dtype);

/**
 * \brief Write the XMF footer.
 *
 * Closes all the XMF tags.
 *
 * \return 1 if it has written everything correctly, 0 otherwise.
 */
int ohmu_hdf5_write_footer (ohmu_hdf5_t *w);

/**
 * \brief Write a single XMF file that describes a time series.
 *
 * The time series is constructed from files named:
 *      basename_%d.xmf
 * as written by the other ohmu_write_* functions. It will write
 * w->files_written entries in the time series corresponding to the number of
 * files that have been written already. The filename of the resulting file
 * is `basename.xmf`.
 */
int ohmu_hdf5_write_xmf_time_series (ohmu_hdf5_t *w);

#endif /* !OHMU_HDF5_H */
