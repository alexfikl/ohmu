// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef OHMU_PARAMETERS_H
#define OHMU_PARAMETERS_H

#include <ohmu_base.h>
#include <ohmu_configreader.h>

typedef enum
{
  OHMU_TIME_FORWARD_EULER,
  OHMU_TIME_GODUNOV_SPLITTING,
  OHMU_TIME_STRANG_SPLITTING,
  OHMU_TIME_SUBCYCLE,
  OHMU_TIME_NONE
} ohmu_time_scheme_t;

/**
 * \brief Parameters of the simulation.
 *
 * All parameters have acceptable values (as mentioned lower) and the init
 * function will abort if the values read from the configuration file are
 * not acceptable.
 */
typedef struct
{
  /**
   * @name Group "iteration".
   */
  /*@{ */
  /**
   * \brief The time stepping scheme to use.
   *
   * Default: OHMU_TIME_FORWARD_EULER. Value has to be in enum.
   */
  ohmu_time_scheme_t time_scheme;

  /**
   * \brief Final time of the simulation.
   *
   * Default: 1.0. Value has to be positive.
   */
  double tmax;

  /**
   * \brief Maximum number of iterations.
   *
   * If positive, the simulation will stop after imax iterations even if tmax
   * was not reached.
   *
   * Default: -1. Can take any integer value.
   */
  int64_t imax;

  /**
   * \brief Courant number.
   *
   * Default: 1.0. Value has to be positive.
   */
  double cfl;

  /**
   * \brief Threshold for refining a quadrant.
   *
   * For each quadrant, the user is expected to compute an indicator between
   * [0, 1]. If this indicator is bigger than epsilon_refine, we should refine.
   *
   * Default: 0.1. Value has to be in [0, 1].
   */
  double epsilon_refine;

  /**
   * \brief Threshold for coarsening a quadrant.
   *
   * For each quadrant, the user is expected to compute an indicator between
   * [0, 1]. If this indicator is smaller than epsilon_coarsen, we should
   * coarsen.
   *
   * Default: 0.1. Value has to be in [0, 1].
   */
  double epsilon_coarsen; /* or coarsen a cell based on a computed
                                           indicator. epsilon_refine should be
                                           bigger or equal to epsilon_coarsen */
  /*@} */

  /**
   * @name Group "plugin".
   */
  /*@} */
  /**
   * \brief Plugin name.
   *
   * Default: NULL. Has to be a non-NULL string.
   */
  const char *plugin_name;
  /*@} */

  /**
   * @name Group "forest".
   */
  /*@} */
  /**
   * \brief Minimum allowed level in the forest during the simulation.
   *
   * Default: 0. Has to be between [0, P4EST_MAXLEVEL].
   */
  int32_t min_allowed_level;

  /**
   * \brief Maximum allowed level in the forest during the simulation.
   *
   * Default: P4EST_MAXLEVEL. Has to be between [0, P4EST_MAXLEVEL] and bigger
   * or equal to min_allowed_level.
   */
  int32_t max_allowed_level;

  /**
   * \brief The name of the connectivity.
   *
   * Default: unit. Has to be a value recognized by p4est_connectivity_byname.
   */
  const char *connectivity_name;
  /*@} */

  /**
   * @name Group "io".
   */
  /*@} */
  /**
   * \brief The filename of the restart file.
   *
   * This file is used to load the initial condition for the simulation and,
   * if the simulation is restarted, save the last state of the forest and
   * user data. The file has to be saved with p4est_save_ext.
   *
   * Default: NULL. Has to be a valid filename.
   */
  const char *restart_file;

  /**
   * \brief Scale for ohmu_hdf5_t.
   *
   * Default: 1.0. Value has to be in [0, 1].
   */
  double mesh_scale;

  /**
   * \brief write_level in ohmu_hdf5_t.
   *
   * Default: 0. Value has to be true / false.
   */
  int mesh_level;

  /**
   * \brief write_rank in ohmu_hdf5_t.
   *
   * Default: 0. Value has to be true / false.
   */
  int mesh_rank;

  /**
   * \brief write_tree in ohmu_hdf5_t.
   *
   * Default: 0. Value has to be true / false.
   */
  int mesh_treeid;

  /**
   * \brief Number of HDF5 output files to write.
   *
   * If the value is negative, no output files are written.
   *
   * Default: 100. Value has to be an integer.
   */
  int outputs;
  /*@} */
} ohmu_parameters_t;

/**
 * \brief Initialize the parameters from the given configuration file.
 *
 * Parameters are checked to have sane values.
 */
void ohmu_parameters_init (ohmu_parameters_t   *params,
                           ohmu_configreader_t *cfg);

#endif /* !OHMU_PARAMETERS_H */
