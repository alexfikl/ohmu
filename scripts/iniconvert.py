#!/usr/bin/python

# SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
# SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
#
# SPDX-License-Identifier: CECILL-2.0

import os
import sys
import configparser


def isnumeric(n):
    try:
        float(n)
        return True
    except ValueError:
        return False


def boolean_to_int (n):
    if n.lower() in ["yes", "true", "y", "t"]:
        return 1
    if n.lower() in ["no", "false", "n", "f"]:
        return 0

    return n


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("usage: {} INI_FILE".format(sys.argv[0]))
        exit(1)

    config = configparser.ConfigParser()
    config.optionxform = lambda option: option
    config.read(sys.argv[1])

    for sec in config.sections():
        clean_sec = sec.replace(" ", "").replace("$", "")
        print("{} = {}".format(clean_sec, "{"))
        for key, value in config.items(sec):
            key = key.replace(" ", "")
            value = boolean_to_int (value)

            if not isnumeric(value):
                print("  {} = \"{}\",".format(key, value))
            else:
                print("  {} = {},".format(key, value))
        print("{}\n".format("}"))
