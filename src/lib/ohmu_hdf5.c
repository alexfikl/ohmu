// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "ohmu_hdf5.h"

#ifndef P4_TO_P8
#include <p4est_nodes.h>
#else
#include <p8est_nodes.h>
#endif

#ifndef P4_TO_P8
#define OHMU_HDF5_NODES_PER_CELL 4
#define OHMU_HDF5_TOPOLOGY "Quadrilateral"
#else
#define OHMU_HDF5_NODES_PER_CELL 8
#define OHMU_HDF5_TOPOLOGY "Hexahedron"
#endif

#ifndef OHMU_MPIRANK_WRAP
#define OHMU_MPIRANK_WRAP 128
#endif

/**
 * \brief Filename template for writing files.
 *
 *    %s        The basename of all files.
 *    %06d      The index of the current file written on 6 spaces with padding
 *    %s        The extension (xmf or h5)
 */
#ifndef OHMU_FILENAME_FORMAT
#define OHMU_FILENAME_FORMAT "%s_%06d"
#endif

/**
 * \brief Convert a H5T_NATIVE_* type to an XDMF NumberType.
 */
const char *
ohmu_hdf5_to_xmf_type (hid_t type)
{
  if (type == H5T_NATIVE_DOUBLE)
  {
    return "NumberType=\"Float\" Precision=\"8\"";
  }
  else if (type == H5T_NATIVE_INT)
  {
    return "NumberType=\"Int\" Precision=\"4\"";
  }
  else if (type == H5T_NATIVE_FLOAT)
  {
    return "NumberType=\"Float\" Precision=\"4\"";
  }
  else if (type == H5T_NATIVE_CHAR)
  {
    return "NumberType=\"Char\"";
  }
  else if (type == H5T_NATIVE_LONG)
  {
    return "NumberType=\"Int\" Precision=\"8\"";
  }
  else if (type == H5T_NATIVE_UCHAR)
  {
    return "NumberType=\"UChar\"";
  }
  else if (type == H5T_NATIVE_UINT)
  {
    return "NumberType=\"UInt\" Precision=\"4\"";
  }
  else if (type == H5T_NATIVE_ULONG)
  {
    return "NumberType=\"UInt\" Precision=\"8\"";
  }

  return NULL;
}

/**
 * \brief Write the coordinates of all the nodes in a quadrant.
 *
 * \param [in] which_tree   The treeid of the current quadrant.
 * \param [in] q            The current quadrants.
 * \param [in,out] vertices An array of size 3 * P4EST_CHILDREN in which we will
 *                          put the (x, y, z) coordinates of each vertex. In
 *                          2D, z is always 0.
 * \param [in] scale        The scale at which to reduce the quadrant.
 */
static void
ohmu_hdf5_quadrant_vertices (p4est_t *p4est, p4est_topidx_t which_tree,
                             p4est_quadrant_t *q, double scale,
                             double *vertices)
{
  double xyz[3] = { 0, 0, 0 };
  double XYZ[3] = { 0, 0, 0 };

  int    k = 0;
  double h = OHMU_QUADRANT_LEN (q->level);

  /* get the coordinates of the lower left corner */
  p4est_qcoord_to_vertex (p4est->connectivity, which_tree, q->x, q->y,
#ifdef P4_TO_P8
                          q->z,
#endif
                          xyz);

#ifdef P4_TO_P8
  for (int zi = 0; zi < 2; ++zi)
  {
#endif
    for (int yi = 0; yi < 2; ++yi)
    {
      for (int xi = 0; xi < 2; ++xi)
      {
        P4EST_ASSERT (0 <= k && k < P4EST_CHILDREN);

        XYZ[0] = xyz[0] + h * (1.0 + (xi * 2 - 1) * scale);
        XYZ[1] = xyz[1] + h * (1.0 + (yi * 2 - 1) * scale);
#ifdef P4_TO_P8
        XYZ[2] = xyz[2] + h * (1.0 + (zi * 2 - 1) * scale);
#endif

        vertices[3 * k + 0] = XYZ[0];
        vertices[3 * k + 1] = XYZ[1];
        vertices[3 * k + 2] = XYZ[2];
        ++k;
      }
    }
#ifdef P4_TO_P8
  }
#endif

  OHMU_ASSERT (k == P4EST_CHILDREN);
}

/**
 * \brief Write the XMF header information: topology and geometry.
 */
static int
ohmu_hdf5_write_xmf_header (ohmu_hdf5_t *w, double time)
{
  if (w->p4est->mpirank != 0)
  { /* only process 0 writes the xmf file */
    return EXIT_SUCCESS;
  }

  FILE *fd = w->xfd;

  fprintf (fd, "<?xml version=\"1.0\" ?>\n");
  fprintf (fd, "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
  fprintf (fd, "<Xdmf Version=\"2.0\">\n");
  fprintf (fd, "  <Domain>\n");
  fprintf (fd, "    <Grid Name=\"%s\" GridType=\"Uniform\">\n", w->file_name);
  fprintf (fd, "      <Time TimeType=\"Single\" Value=\"%g\" />\n", time);

  /* Connectivity */
  fprintf (fd,
           "      <Topology TopologyType=\"%s\" NumberOfElements=\"%lu\""
           ">\n",
           OHMU_HDF5_TOPOLOGY, w->quads.dims[0]);
  fprintf (fd,
           "        <DataItem Dimensions=\"%lu %d\" DataType=\"Int\""
           " Format=\"HDF\">\n",
           w->quads.dims[0], OHMU_HDF5_NODES_PER_CELL);
  fprintf (fd, "         %s.h5:/connectivity\n", w->file_name);
  fprintf (fd, "        </DataItem>\n");
  fprintf (fd, "      </Topology>\n");

  /* Points */
  fprintf (fd, "      <Geometry GeometryType=\"XYZ\">\n");
  fprintf (fd,
           "        <DataItem Dimensions=\"%lu 3\" NumberType=\"Float\""
           " Precision=\"8\" Format=\"HDF\">\n",
           w->nodes.dims[0]);
  fprintf (fd, "         %s.h5:/coordinates\n", w->file_name);
  fprintf (fd, "        </DataItem>\n");
  fprintf (fd, "      </Geometry>\n");

  return EXIT_SUCCESS;
}

/**
 * \brief Write information about an attribute.
 *
 * \param [in] name The name of the attribute.
 * \param [in] type The type.
 * \param [in] dims The dimensions of the attribute. If it is a scalar,
 * dims[1] will be ignored.
 */
static int
ohmu_hdf5_write_xmf_attribute (ohmu_hdf5_t *w, const char *name, hid_t dtype,
                               ohmu_hdf5_attribute_type_t type,
                               hsize_t                    dims[2])
{
  if (w->p4est->mpirank != 0)
  {
    return EXIT_SUCCESS;
  }

  FILE *fd = w->xfd;

  fprintf (fd, "      <Attribute Name=\"%s\"", name);
  switch (type)
  {
  case OHMU_HDF5_CELL_SCALAR:
    fprintf (fd, " AttributeType=\"Scalar\" Center=\"Cell\">\n");
    break;
  case OHMU_HDF5_CELL_VECTOR:
    fprintf (fd, " AttributeType=\"Vector\" Center=\"Cell\">\n");
    break;
  case OHMU_HDF5_NODE_SCALAR:
    fprintf (fd, " AttributeType=\"Scalar\" Center=\"Node\">\n");
    break;
  case OHMU_HDF5_NODE_VECTOR:
    fprintf (fd, " AttributeType=\"Vector\" Center=\"Node\">\n");
    break;
  default:
    OHMU_LERROR ("Unsupported field type.\n");
    return 0;
  }

  fprintf (fd, "        <DataItem %s Format=\"HDF\"",
           ohmu_hdf5_to_xmf_type (dtype));
  if (type == OHMU_HDF5_CELL_SCALAR || type == OHMU_HDF5_NODE_SCALAR)
  {
    fprintf (fd, " Dimensions=\"%lu\">\n", dims[0]);
  }
  else
  {
    fprintf (fd, " Dimensions=\"%lu %lu\">\n", dims[0], dims[1]);
  }

  fprintf (fd, "         %s.h5:/%s\n", w->file_name, name);
  fprintf (fd, "        </DataItem>\n");
  fprintf (fd, "      </Attribute>\n");

  return EXIT_SUCCESS;
}

/**
 * \brief Close all remaining tags.
 */
static int
ohmu_hdf5_write_xmf_footer (ohmu_hdf5_t *w)
{
  if (w->p4est->mpirank != 0)
  {
    return EXIT_SUCCESS;
  }

  FILE *fd = w->xfd;

  fprintf (fd, "    </Grid>\n");
  fprintf (fd, "  </Domain>\n");
  fprintf (fd, "</Xdmf>\n");

  return EXIT_SUCCESS;
}

/**
 * \brief Write a given dataset into the HDF5 file.
 *
 * \param [in] fd An open file descriptor to a HDF5 file.
 * \param [in] name The name of the dataset we are writing.
 * \param [in] data The data to write.
 * \param [in] type_id The native HDF5 type of the data. See H5Tpublic.h
 * \param [in] rank The rank of the dataset. 1 if it is a vector, 2 for a matrix.
 * \param [in] dims The global dimensions of the dataset.
 * \param [in] count The local dimensions of the dataset.
 * \param [in] start The offset of the local data with respect to the global
 * positioning.
 */
static void
ohmu_hdf5_write_dataset (ohmu_hdf5_t *w, const char *name, const void *data,
                         ohmu_hdf5_dataset_t ds)
{
  hid_t filespace = 0;
  hid_t memspace = 0;
  hid_t dataset = 0;
  hid_t properties = 0;

  hid_t fd = w->hfd;

  // create the layout in the file and in the memory of the current process
  filespace = H5Screate_simple (ds.rank, ds.dims, NULL);
  memspace = H5Screate_simple (ds.rank, ds.count, NULL);

  // create the dataset and the location of the local data
  dataset = H5Dcreate2 (fd, name, ds.type, filespace, H5P_DEFAULT, H5P_DEFAULT,
                        H5P_DEFAULT);
  H5Sselect_hyperslab (filespace, H5S_SELECT_SET, ds.start, NULL, ds.count,
                       NULL);

  // set some properties
  properties = H5Pcreate (H5P_DATASET_XFER);
  H5Pset_dxpl_mpio (properties, H5FD_MPIO_COLLECTIVE);

  // write the data
  H5Dwrite (dataset, ds.type, memspace, filespace, properties, data);

  H5Dclose (dataset);
  H5Sclose (filespace);
  H5Sclose (memspace);
  H5Pclose (properties);
}

/**
 * \brief Compute and write the coordinates of all the mesh nodes.
 */
static void
ohmu_hdf5_write_coordinates (ohmu_hdf5_t *w, p4est_nodes_t *nodes)
{
  p4est_t            *p4est = w->p4est;
  ohmu_hdf5_dataset_t ds = w->nodes;

  sc_array_t         *trees = p4est->trees;
  p4est_tree_t       *tree = NULL;
  sc_array_t         *quadrants = NULL;
  p4est_quadrant_t   *q = NULL;
  p4est_indep_t      *indep = NULL;

  p4est_topidx_t      first_local_tree = p4est->first_local_tree;
  p4est_topidx_t      last_local_tree = p4est->last_local_tree;
  p4est_locidx_t      offset = 0;
  p4est_topidx_t      jt = 0;

  double              xyz[3] = { 0, 0, 0 };
  double             *data = OHMU_ALLOC (double, 3 * ds.count[0]);

  /* setup the right dataset */
  ds.type = H5T_NATIVE_DOUBLE;
  ds.rank = 2;
  ds.dims[1] = 3;
  ds.count[1] = 3;
  ds.start[1] = 0;

  /* construct the list of node coordinates */
  if (nodes == NULL)
  {
    for (jt = first_local_tree; jt <= last_local_tree; ++jt)
    {
      tree = p4est_tree_array_index (trees, jt);
      quadrants = &(tree->quadrants);

      for (size_t jq = 0; jq < quadrants->elem_count; ++jq)
      {
        q = p4est_quadrant_array_index (quadrants, jq);

        /* add the coordinates of the current quad to data */
        ohmu_hdf5_quadrant_vertices (p4est, jt, q, w->scale, data + offset);

        /* go to the next quadrant */
        offset += 3 * P4EST_CHILDREN;
      }
    }
  }
  else
  {
    for (size_t jl = 0; jl < nodes->indep_nodes.elem_count; ++jl)
    {
      indep = (p4est_indep_t *)sc_array_index (&(nodes->indep_nodes), jl);

      /* retrieve corners of the tree */
      p4est_qcoord_to_vertex (p4est->connectivity, indep->p.which_tree,
                              indep->x, indep->y,
#ifdef P4_TO_P8
                              indep->z,
#endif
                              xyz);

      data[3 * jl + 0] = xyz[0];
      data[3 * jl + 1] = xyz[1];
      data[3 * jl + 2] = xyz[2];
    }
  }

  /* write the data */
  ohmu_hdf5_write_dataset (w, "coordinates", data, ds);

  P4EST_FREE (data);
}

/**
 * \brief Compute and write the connectivity information for each quadrant.
 */
static int
ohmu_hdf5_write_connectivity (ohmu_hdf5_t *w, p4est_nodes_t *nodes)
{
  const p4est_locidx_t node_order[P4EST_CHILDREN] = { 0,
                                                      1,
                                                      3,
                                                      2
#ifdef P4_TO_P8
                                                      ,
                                                      4,
                                                      5,
                                                      7,
                                                      6
#endif
  };

  ohmu_hdf5_dataset_t ds = w->quads;

  p4est_locidx_t      jn = 0;
  p4est_locidx_t      k = 0;
  size_t              offset = 0;

  int                *data = P4EST_ALLOC (int, P4EST_CHILDREN *ds.count[0]);

  /* setup the right dataset */
  ds.type = H5T_NATIVE_INT;
  ds.rank = 2;
  ds.dims[1] = P4EST_CHILDREN;
  ds.count[1] = P4EST_CHILDREN;
  ds.start[1] = 0;

  /* get connectivity data */
  for (size_t jl = 0; jl < ds.count[0]; ++jl)
  {
    for (size_t jc = 0; jc < P4EST_CHILDREN; ++jc)
    {
      k = P4EST_CHILDREN * jl + jc;
      offset = jn + node_order[jc];

      if (nodes == NULL)
      {
        data[k] = w->nodes.start[0] + offset;
      }
      else
      {
        data[k] = w->nodes.start[0] + nodes->local_nodes[offset];
      }
    }

    jn += P4EST_CHILDREN;
  }

  /* write the connectivity */
  ohmu_hdf5_write_dataset (w, "coordinates", data, ds);

  P4EST_FREE (data);
  return EXIT_SUCCESS;
}

/**
 * \brief Get and write the tree id for each quadrant.
 */
static int
ohmu_hdf5_write_tree (ohmu_hdf5_t *w)
{
  if (!w->write_tree)
  {
    return EXIT_SUCCESS;
  }

  p4est_t       *p4est = w->p4est;

  p4est_tree_t  *tree = NULL;
  p4est_topidx_t first_local_tree = p4est->first_local_tree;
  p4est_topidx_t last_local_tree = p4est->last_local_tree;

  p4est_locidx_t jl = 0;
  int           *data = OHMU_ALLOC (int, w->quads.count[0]);

  /* gather the tree id for each local quadrant */
  for (p4est_topidx_t jt = first_local_tree; jt <= last_local_tree; ++jt)
  {
    tree = p4est_tree_array_index (p4est->trees, jt);

    for (size_t jq = 0; jq < tree->quadrants.elem_count; ++jq, ++jl)
    {
      data[jl] = jt;
    }
  }

  ohmu_hdf5_write_attribute (w, "treeid", data, 1, OHMU_HDF5_CELL_SCALAR,
                             H5T_NATIVE_INT);

  OHMU_FREE (data);
  return EXIT_SUCCESS;
}

/**
 * \brief Get and write the level for each quadrant.
 */
static int
ohmu_hdf5_write_level (ohmu_hdf5_t *w)
{
  if (!w->write_level)
  {
    return EXIT_SUCCESS;
  }

  p4est_t          *p4est = w->p4est;

  p4est_tree_t     *tree = NULL;
  sc_array_t       *quadrants = NULL;
  p4est_quadrant_t *q = NULL;

  p4est_topidx_t    first_local_tree = p4est->first_local_tree;
  p4est_topidx_t    last_local_tree = p4est->last_local_tree;

  p4est_locidx_t    jl = 0;
  int              *data = OHMU_ALLOC (int, w->quads.count[0]);

  /* gather the level for each local quadrant */
  for (p4est_topidx_t jt = first_local_tree; jt <= last_local_tree; ++jt)
  {
    tree = p4est_tree_array_index (p4est->trees, jt);
    quadrants = &(tree->quadrants);

    for (size_t jq = 0; jq < quadrants->elem_count; ++jq, ++jl)
    {
      q = p4est_quadrant_array_index (quadrants, jq);
      data[jl] = q->level;
    }
  }

  ohmu_hdf5_write_attribute (w, "level", data, 1, OHMU_HDF5_CELL_SCALAR,
                             H5T_NATIVE_INT);

  OHMU_FREE (data);
  return EXIT_SUCCESS;
}

/**
 * \brief Compute and write the MPI rank for each quadrant.
 *
 * The rank is wrapped with OHMU_MPIRANK_WRAP.
 */
static int
ohmu_hdf5_write_rank (ohmu_hdf5_t *w)
{
  if (!w->write_rank)
  {
    return EXIT_SUCCESS;
  }

  p4est_t       *p4est = w->p4est;

  p4est_tree_t  *tree = NULL;
  p4est_topidx_t first_local_tree = p4est->first_local_tree;
  p4est_topidx_t last_local_tree = p4est->last_local_tree;

  p4est_locidx_t jl = 0;
  int           *data = OHMU_ALLOC (int, w->quads.count[0]);

  /* compute wrapped mpirank */
  int mpirank = p4est->mpirank % OHMU_MPIRANK_WRAP;

  /* all local quads have the same mpirank */
  for (p4est_topidx_t jt = first_local_tree; jt <= last_local_tree; ++jt)
  {
    tree = p4est_tree_array_index (p4est->trees, jt);

    for (size_t jq = 0; jq < tree->quadrants.elem_count; ++jq, ++jl)
    {
      data[jl] = mpirank;
    }
  }
  P4EST_ASSERT (jl == w->quads.count[0]);

  ohmu_hdf5_write_attribute (w, "mpirank", data, 0, OHMU_HDF5_CELL_SCALAR,
                             H5T_NATIVE_INT);

  OHMU_FREE (data);
  return EXIT_SUCCESS;
}

ohmu_hdf5_t *
ohmu_hdf5_writer_new (p4est_t *p4est, const char *name)
{
  return ohmu_hdf5_new_ext (p4est, name, 1.0, 1, 1, 1);
}

ohmu_hdf5_t *
ohmu_hdf5_new_ext (p4est_t *p4est, const char *name, double scale,
                   int write_level, int write_tree, int write_rank)
{
  ohmu_hdf5_t *w = OHMU_ALLOC_ZERO (ohmu_hdf5_t, 1);

  w->p4est = p4est;
  w->file_base = OHMU_STRDUP (name);
  w->file_index = 0;

  w->scale = scale;
  w->write_level = write_level;
  w->write_tree = write_tree;
  w->write_rank = write_rank;

  return w;
}

void
ohmu_hdf5_destroy (ohmu_hdf5_t *w)
{
  OHMU_FREE (w->file_base);
  OHMU_FREE (w);
}

int
ohmu_hdf5_write_mesh (p4est_t *p4est, const char *name)
{
  ohmu_hdf5_t *w = NULL;

  w = ohmu_hdf5_new_ext (p4est, name, 0.95, 1, 1, 1);
  ohmu_hdf5_write_header (w, 0);
  ohmu_hdf5_write_footer (w);
  ohmu_hdf5_destroy (w);

  return 1;
}

/**
 * \brief Set sizes of a dataset for quadrant (cell) data.
 *
 * It supposes that the dataset is 2D, the first dimension is populated with
 * information from p4est concerning the local and global number of quadrants.
 * The second dimension info is set with the extra arguments.
 *
 * \param[in] dim   If the dataset is 2D, this should be the size of the second
 *                  dimension.
 * \param[in] count The size of the second dimension.
 * \param[in] start The offset for the second dimension.
 */
static void
ohmu_hdf5_dataset_quadrants (ohmu_hdf5_t *w, p4est_t *p4est, hsize_t dim,
                             hsize_t count, hsize_t start)
{
  int mpirank = p4est->mpirank;

  w->quads.dims[0] = p4est->global_num_quadrants;
  w->quads.dims[1] = dim;

  w->quads.count[0] = p4est->local_num_quadrants;
  w->quads.count[1] = count;

  w->quads.start[0] = p4est->global_first_quadrant[mpirank];
  w->quads.start[1] = start;
}

/**
 * \brief Set sizes of a dataset for node (point) data.
 *
 * It supposes that the dataset is 2D, the first dimension is populated with
 * information from p4est concerning the local and global number of nodes. If
 * the scale of each quadrant is == 1, we use p4est_nodes_t to get a unique
 * numbering of all independent nodes.
 *
 * The second dimension info is set with the extra arguments.
 *
 * \param[in] dim   If the dataset is 2D, this should be the size of the second
 *                  dimension.
 * \param[in] count The size of the second dimension.
 * \param[in] start The offset for the second dimension.
 * \return If created, the p4est_nodes_t structure, otherwise NULL.
 */
static p4est_nodes_t *
ohmu_hdf5_dataset_nodes (ohmu_hdf5_t *w, p4est_t *p4est, hsize_t dim,
                         hsize_t count, hsize_t start)
{
  int            mpirank = p4est->mpirank;
  sc_MPI_Comm    mpicomm = p4est->mpicomm;
  p4est_nodes_t *nodes = NULL;

  if (w->scale < 0)
  {
    w->nodes.dims[0] = P4EST_CHILDREN * p4est->global_num_quadrants;
    w->nodes.count[0] = P4EST_CHILDREN * p4est->local_num_quadrants;
    w->nodes.start[0] = P4EST_CHILDREN * p4est->global_first_quadrant[mpirank];
  }
  else
  {
    nodes = p4est_nodes_new (p4est, NULL);

    /* get the number of nodes in each process */
    nodes->global_owned_indeps = P4EST_ALLOC (p4est_locidx_t, p4est->mpisize);
    nodes->num_owned_indeps = nodes->indep_nodes.elem_count;
    nodes->global_owned_indeps[mpirank] = nodes->indep_nodes.elem_count;

    sc_MPI_Allgather (&(nodes->num_owned_indeps), 1, P4EST_MPI_LOCIDX,
                      nodes->global_owned_indeps, 1, P4EST_MPI_LOCIDX,
                      mpicomm);

    /* compute the offset and total number of nodes */
    nodes->offset_owned_indeps = 0;
    w->nodes.dims[0] = 0;
    for (int i = 0; i < p4est->mpisize; ++i)
    {
      if (i < mpirank)
      {
        nodes->offset_owned_indeps += nodes->global_owned_indeps[i];
      }

      w->nodes.dims[0] += nodes->global_owned_indeps[i];
    }

    w->nodes.count[0] = nodes->indep_nodes.elem_count;
    w->nodes.start[0] = nodes->offset_owned_indeps;
  }

  w->nodes.dims[1] = dim;
  w->nodes.count[1] = count;
  w->nodes.start[1] = start;

  return nodes;
}

int
ohmu_hdf5_write_header (ohmu_hdf5_t *w, double time)
{
  p4est_t       *p4est = w->p4est;
  p4est_nodes_t *nodes = NULL;

  hid_t          plist;
  char           filename[BUFSIZ];

  /* get the number of quadrants to write */
  ohmu_hdf5_dataset_quadrants (w, p4est, 1, 1, 1);

  /* get the number of nodes to write */
  nodes = ohmu_hdf5_dataset_nodes (w, p4est, 1, 1, 1);

  /* open the corresponding files */
  snprintf (filename, BUFSIZ, OHMU_FILENAME_FORMAT, w->file_base,
            w->file_index);
  w->file_name = P4EST_STRDUP (filename);

  if (p4est->mpirank == 0)
  {
    snprintf (filename, BUFSIZ, "%s.xmf", w->file_name);
    w->xfd = fopen (filename, "w");
    SC_CHECK_ABORTF (w->xfd != NULL, "Could not open file \"%s\".",
                     w->file_name);
  }

  snprintf (filename, BUFSIZ, "%s.h5", w->file_name);
  plist = H5Pcreate (H5P_FILE_ACCESS);
  H5Pset_fapl_mpio (plist, w->p4est->mpicomm, MPI_INFO_NULL);
  w->hfd = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, plist);
  SC_CHECK_ABORTF (w->hfd >= 0, "Could not open file \"%s\"", filename);

  H5Pclose (plist);

  /* write the geometry first */
  ohmu_hdf5_write_xmf_header (w, time);
  ohmu_hdf5_write_coordinates (w, nodes);
  ohmu_hdf5_write_connectivity (w, nodes);

  if (nodes != NULL)
  {
    p4est_nodes_destroy (nodes);
  }

  /* write the extra per-quadrant data, if set to true in ohmu_hdf5_writer_t */
  ohmu_hdf5_write_tree (w);
  ohmu_hdf5_write_level (w);
  ohmu_hdf5_write_rank (w);

  return 1;
}

int
ohmu_hdf5_write_attribute (ohmu_hdf5_t *w, const char *name, void *data,
                           size_t dim, ohmu_hdf5_attribute_type_t ftype,
                           hid_t dtype)
{
  int                 retval = EXIT_SUCCESS;
  ohmu_hdf5_dataset_t ds;

  if (ftype == OHMU_HDF5_CELL_SCALAR || ftype == OHMU_HDF5_CELL_VECTOR)
  {
    ds = w->quads;
  }
  else
  {
    ds = w->nodes;
  }

  if (ftype == OHMU_HDF5_CELL_SCALAR || ftype == OHMU_HDF5_NODE_SCALAR)
  {
    ds.rank = 1;
  }
  else
  {
    ds.rank = 2;
  }

  ds.type = dtype;
  ds.dims[1] = dim;
  ds.count[1] = dim;
  ds.start[1] = 0;

  /* write the data to the xmf and h5 files */
  ohmu_hdf5_write_xmf_attribute (w, name, dtype, ftype, ds.dims);
  ohmu_hdf5_write_dataset (w, name, data, ds);

  return retval;
}

int
ohmu_hdf5_write_footer (ohmu_hdf5_t *w)
{
  ohmu_hdf5_write_xmf_footer (w);

  if (w->xfd != NULL)
  {
    fclose (w->xfd);
    w->xfd = NULL;
  }

  if (w->hfd >= 0)
  {
    H5Fflush (w->hfd, H5F_SCOPE_GLOBAL);
    H5Fclose (w->hfd);
    w->hfd = 0;
  }

  if (w->file_name != NULL)
  {
    P4EST_FREE (w->file_name);
    w->file_name = NULL;
  }

  w->file_index += 1;
  return EXIT_SUCCESS;
}

int
ohmu_hdf5_write_xmf_time_series (ohmu_hdf5_t *w)
{
  if (w->p4est->mpirank != 0)
  {
    return EXIT_SUCCESS;
  }

  FILE *fd;
  char  filename[BUFSIZ];

  snprintf (filename, BUFSIZ, "%s.xmf", w->file_base);
  fd = fopen (filename, "w");
  SC_CHECK_ABORTF (fd != NULL, "Could not create file \"%s\".", filename);

  fprintf (fd, "<?xml version=\"1.0\" ?>\n");
  fprintf (fd, "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
  fprintf (fd, "<Xdmf xmlns:xi=\"http://www.w3.org/2001/XInclude\""
               " Version=\"2.0\">\n");
  fprintf (fd, "  <Domain Name=\"MainTimeSeries\">\n");
  fprintf (fd, "    <Grid Name=\"MainTimeSeries\" GridType=\"Collection\""
               " CollectionType=\"Temporal\">\n");

  /* include all the other written files. */
  for (int i = 0; i < w->file_index; ++i)
  {
    snprintf (filename, BUFSIZ, OHMU_FILENAME_FORMAT ".xmf", w->file_base, i);
    fprintf (fd,
             "      <xi:include href=\"%s\""
             " xpointer=\"xpointer(//Xdmf/Domain/Grid)\" />\n",
             filename);
  }

  fprintf (fd, "    </Grid>\n");
  fprintf (fd, "  </Domain>\n");
  fprintf (fd, "</Xdmf>\n");
  fclose (fd);

  return EXIT_SUCCESS;
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
