// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "ohmu_quadrant.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#else
#include <p8est_bits.h>
#endif

/**
 * \brief Get a quadrant from the forest.
 *
 * This function is used with data from p4est_mesh_t, in particular with
 * the quadid provided by p4est_mesh_t->quad_to_quad. This id encodes
 * whether the quadrant is in the ghost layer or in the local process.
 *
 * The returned quadrant has its pad8 member set to 0 or 1, whether it is
 * a ghost or not. Also, the quadid and treeid arguments are modified
 * to index into the right array directly. treeid is set to the
 * piggy3.which_tree member of the ghost quadrant (if it is a ghost quadrant)
 * which is set by p4est_ghost_new when constructing the ghost layer.
 *
 * \param [in,out] quadid The quadrant id as available in quad_to_quad. If
 *                     the quadrant is in the ghost layer, the id is modified
 *                     to index directly into it.
 * \param [in,out] treeid The tree id as available in quad_to_tree. If
 *                            it is a ghost quadrant, it is modified to the
 *                            contents of piggy3.which_tree.
 */
static p4est_quadrant_t *
ohmu_mesh_get_quadrant (p4est_t *p4est, p4est_mesh_t *mesh,
                        p4est_ghost_t *ghost_layer, p4est_locidx_t *quadid,
                        p4est_topidx_t *treeid)
{
  p4est_locidx_t    lnq = p4est->local_num_quadrants;
  p4est_quadrant_t *q = NULL;

  if (*quadid < lnq)
  { // is local
    q = p4est_mesh_quadrant_cumulative (p4est, mesh, *quadid, treeid, quadid);
    q->pad8 = 0;
  }
  else
  { // is ghost
    *quadid -= lnq;

    q = p4est_quadrant_array_index (&ghost_layer->ghosts, *quadid);
    q->pad8 = 1;
    *treeid = q->p.piggy3.which_tree;
  }

  return q;
}

void
ohmu_quadrant_print (int log_priority, p4est_quadrant_t *q)
{
  p4est_qcoord_t x = (q->x) >> (P4EST_MAXLEVEL - q->level);
  p4est_qcoord_t y = (q->y) >> (P4EST_MAXLEVEL - q->level);
#ifdef P4_TO_P8
  p4est_qcoord_t z = (q->z) >> (P4EST_MAXLEVEL - q->level);

  OHMU_LOGF (log_priority, "x %d y %d z %d level %d\n", x, y, z, q->level);
#else
  OHMU_LOGF (log_priority, "x %d y %d level %d\n", x, y, q->level);
#endif
}

double
ohmu_quadrant_length (p4est_quadrant_t *q)
{
  return (double)P4EST_QUADRANT_LEN (q->level) / P4EST_ROOT_LEN;
}

void
ohmu_quadrant_center_vertex (p4est_connectivity_t *connectivity,
                             p4est_topidx_t tree, p4est_quadrant_t *q,
                             double vxyz[3])
{
  p4est_qcoord_t half = P4EST_QUADRANT_LEN (q->level) / 2;

  p4est_qcoord_to_vertex (connectivity, tree, q->x + half, q->y + half,
#ifdef P4_TO_P8
                          q->z + half,
#endif
                          vxyz);
}

int8_t
ohmu_face_side_level (p4est_iter_face_side_t *side)
{
  if (side->is_hanging)
  {
    return side->is.hanging.quad[0]->level;
  }

  return side->is.full.quad->level;
}

p4est_quadrant_t *
ohmu_face_side_quadrant (p4est_iter_face_side_t *side, int8_t idx,
                         p4est_locidx_t *quadid)
{
  // the maximum index for a side is P4EST_HALF (so 2 or 4, based on the dim)
  // if the face is hanging, and 1 if it is a normal face
  int               max_idx = (side->is_hanging ? P4EST_HALF : 1);
  p4est_quadrant_t *q = NULL;

  // return if the requested index does not exist
  if (idx >= max_idx)
  {
    return q;
  }

  // get the correct quad
  // NOTE: the quad can sometimes be NULL. see p4est_iterate.h
  if (side->is_hanging)
  {
    q = side->is.hanging.quad[idx];
    SC_CHECK_ABORT (q != NULL, "quad is null");

    q->pad8 = side->is.hanging.is_ghost[idx] ? 1 : 0;
    if (quadid != NULL)
    {
      *quadid = side->is.hanging.quadid[idx];
    }
  }
  else
  {
    q = side->is.full.quad;
    SC_CHECK_ABORT (q != NULL, "quad is null");

    q->pad8 = side->is.full.is_ghost ? 1 : 0;
    if (quadid != NULL)
    {
      *quadid = side->is.full.quadid;
    }
  }

  return q;
}

p4est_iter_face_side_t
ohmu_quadrant_face_neighbor (p4est_wrap_t *pp, p4est_topidx_t treeid,
                             p4est_locidx_t quadid, int face)
{
  p4est_t               *p4est = pp->p4est;
  p4est_ghost_t         *ghosts = p4est_wrap_get_ghost (pp);
  p4est_mesh_t          *mesh = p4est_wrap_get_mesh (pp);
  p4est_tree_t          *tree = p4est_tree_array_index (p4est->trees, treeid);

  p4est_topidx_t         ntree = 0;
  p4est_locidx_t         facecode = 0;
  p4est_locidx_t         qtq = 0;
  p4est_locidx_t        *halves = NULL;

  p4est_quadrant_t      *q = NULL;
  p4est_iter_face_side_t side;

  // get the id of the neighboring quad
  facecode = P4EST_FACES * (tree->quadrants_offset + quadid) + face;
  qtq = mesh->quad_to_quad[facecode];

  side.is_hanging = mesh->quad_to_face[facecode] < 0;
  if (side.is_hanging)
  { // is a half face
    // get the indices of the 2 (4) smaller quadrants
    halves = sc_array_index (mesh->quad_to_half, qtq);

    // put them in the side with the right information
    for (int i = 0; i < P4EST_HALF; ++i)
    {
      quadid = halves[i];
      q = ohmu_mesh_get_quadrant (p4est, mesh, ghosts, &quadid, &ntree);

      side.is.hanging.is_ghost[i] = q->pad8;
      side.is.hanging.quad[i] = q;
      side.is.hanging.quadid[i] = quadid;
    }
  }
  else
  { // is a full face
    quadid = qtq;
    q = ohmu_mesh_get_quadrant (p4est, mesh, ghosts, &quadid, &treeid);

    side.is.full.is_ghost = q->pad8;
    side.is.full.quad = q;
    side.is.full.quadid = quadid;
  }

  side.treeid = ntree;
  side.face = face;

  return side;
}

/*
void
ohmu_mesh_corner_neighbor_init (ohmu_mesh_corner_neighbor_t *mcn,
                                p4est_t * p4est,
                                p4est_ghost_t * ghost,
                                p4est_mesh_t * mesh,
                                p4est_topidx_t which_tree;
                                p4est_locidx_t quadrant_id)
{
  p4est_tree_t       *tree;

  mcn->p4est = p4est;
  mcn->ghost = ghost;
  mcn->mesh = mesh;

  P4EST_ASSERT (0 <= which_tree &&
                which_tree < p4est->connectivity->num_trees);
  mcn->which_tree = which_tree;
  tree = p4est_tree_array_index (p4est->trees, which_tree);

  P4EST_ASSERT (0 <= quadrant_id &&
                (size_t) quadrant_id < tree->quadrants.elem_count);
  mcn->quadrant_id = quadrant_id;
  mcn->quadrant_code = P4EST_FACES * (tree->quadrants_offset + quadrant_id);

  mcn->corner = -1;
  mcn->face = 0;
  mcn->subface = 0;
  mcn->current_qtq = -1;
}

p4est_quadrant_t *
ohmu_mesh_corner_neighbor_next (ohmu_mesh_corner_neighbor_t *mcn,
                                p4est_topidx_t *ntree,
                                p4est_locidx_t *nquad,
                                int *ncorner, int *nface, int *nrank)
{

}

void *
ohmu_mesh_corner_neighbor_data (ohmu_mesh_corner_neighbor_t *mcn,
                                void * ghost_data)
{
  p4est_locidx_t      qtq = mcn->current_qtq;
  p4est_locidx_t      lnq = mcn->mesh->local_num_quadrants;
  size_t              data_size = mcn->p4est->data_size;

  P4EST_ASSERT (qtq >= 0);

  if (qtq < lnq) {
    p4est_topidx_t      which_tree;
    p4est_quadrant_t   *q;
    which_tree = mcn->which_tree;
    q = p4est_mesh_quadrant_cumulative (mcn->p4est, qtq, &which_tree, NULL);
    return q->p.user_data;
  }
  else {
    qtq -= lnq;
    return ((char *) ghost_data + data_size * qtq);
  }
}

p4est_quadrant_t   *
ohmu_quadrant_corner_neighbor (p4est_t * p4est,
                               p4est_ghost_t * ghost,
                               p4est_mesh_t * mesh,
                               p4est_topidx_t * treeid,
                               p4est_locidx_t * quadid,
                               int corner)
{
  p4est_quadrant_t   *q = NULL;
  p4est_locidx_t      lnq = p4est->local_num_quadrants;
  p4est_tree_t       *tree = p4est_tree_array_index (p4est->trees, *treeid);

  p4est_locidx_t      qtc = 0;
  p4est_locidx_t      cornerid = 0;

  cornerid = P4EST_FACES * (tree->quadrants_offset + *quadid) + corner;
  qtc = mesh->quad_to_corner[cornerid];

  if (qtc < 0) {
    return NULL;
  }

  if (qtc < lnq) {
    q = p4est_mesh_quadrant_cumulative (p4est, qtc, treeid, NULL);
    q->pad8 = 0;
  }
  else {
    qtc -= lnq;

    q = p4est_quadrant_array_index (&ghost->ghosts, qtc);
    q->pad8 = 1;
  }

  return q;
}*/

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
