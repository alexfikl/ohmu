// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef OHMU_BASE_H
#define OHMU_BASE_H

#include <ohmu_config.h>

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#include <p4est_ghost.h>
#else
#include <p8est.h>
#include <p8est_ghost.h>
#endif

#include <sc.h>
#include <sc_containers.h>

#ifndef OHMU_THRESHOLD
#define OHMU_THRESHOLD 1e-12
#endif

/* Some extra useful macros */
#define OHMU_UNUSED(x) ((void)(x))
#define OHMU_FUZZY_NULL(a) (fabs (a) < OHMU_THRESHOLD)
#define OHMU_FUZZY_COMPARE(a, b)                                              \
  ((OHMU_FUZZY_NULL (a) && OHMU_FUZZY_NULL (b))                               \
   || (fabs ((a) - (b)) * 1000000000000. <= SC_MIN (fabs (a), fabs (b))))
#define OHMU_FUZZY_LIMITS(x, a, b)                                            \
  ((x) > ((a) - OHMU_THRESHOLD) && (x) < ((b) + OHMU_THRESHOLD))

#define OHMU_QUADRANT_LEN(l) ((double)P4EST_QUADRANT_LEN (l) / P4EST_ROOT_LEN)

extern int ohmu_package_id;

/**
 * \brief Initialize specific ohmu loggers.
 *
 * This also initializes the libsc and p4est loggers with the same handler
 * and threshold as the ohmu logger.
 *
 * \sa p4est_init() and sc_init().
 */
void ohmu_init (MPI_Comm mpicomm, sc_log_handler_t log_handler,
                sig_t sig_handler, int log_threshold);

/**
 * \brief Finalize all the loggers and check the memory status.
 *
 * \sa sc_finalize
 */
void ohmu_finalize (void);

/**
 * \brief Get the version of the library.
 *
 * It constructs the version structure from the values defined in ohmu_config.h.
 */
ohmu_version_t ohmu_version (void);

/**
 * \brief Test whether a file is empty or not
 *
 * \return true if it is empty, false otherwise.
 */
int ohmu_file_empty (FILE *fd);

/*****************************************************************************
 * The rest is directly copied from p4est_base.h with removed support for C++
 * and renamed to our library
 ****************************************************************************/

/* some error checking possibly specific to ohmu */
#ifdef OHMU_DEBUG
#define OHMU_ASSERT(c) SC_CHECK_ABORT ((c), "Assertion '" #c "'")
#define OHMU_EXECUTE_ASSERT_FALSE(expression)                                 \
  do                                                                          \
  {                                                                           \
    int _ohmu_i = (int)(expression);                                          \
    SC_CHECK_ABORT (!_ohmu_i, "Expected false: '" #expression "'");           \
  } while (0)
#define OHMU_EXECUTE_ASSERT_TRUE(expression)                                  \
  do                                                                          \
  {                                                                           \
    int _ohmu_i = (int)(expression);                                          \
    SC_CHECK_ABORT (_ohmu_i, "Expected true: '" #expression "'");             \
  } while (0)
#else
#define OHMU_ASSERT(c) SC_NOOP ()
#define OHMU_EXECUTE_ASSERT_FALSE(expression)                                 \
  do                                                                          \
  {                                                                           \
    (void)(expression);                                                       \
  } while (0)
#define OHMU_EXECUTE_ASSERT_TRUE(expression)                                  \
  do                                                                          \
  {                                                                           \
    (void)(expression);                                                       \
  } while (0)
#endif

/* macros for memory allocation, will abort if out of memory */
#define OHMU_ALLOC(t, n) (t *)sc_malloc (ohmu_package_id, (n) * sizeof (t))
#define OHMU_ALLOC_ZERO(t, n)                                                 \
  (t *)sc_calloc (ohmu_package_id, (size_t)(n), sizeof (t))
#define OHMU_REALLOC(p, t, n)                                                 \
  (t *)sc_realloc (ohmu_package_id, (p), (n) * sizeof (t))
#define OHMU_STRDUP(s) sc_strdup (ohmu_package_id, (s))
#define OHMU_FREE(p) sc_free (ohmu_package_id, (p))

/* log helper macros */
#define OHMU_GLOBAL_LOG(p, s)                                                 \
  SC_GEN_LOG (ohmu_package_id, SC_LC_GLOBAL, (p), (s))
#define OHMU_LOG(p, s) SC_GEN_LOG (ohmu_package_id, SC_LC_NORMAL, (p), (s))
#define OHMU_GLOBAL_LOGF(p, f, ...)                                           \
  SC_GEN_LOGF (ohmu_package_id, SC_LC_GLOBAL, (p), (f), __VA_ARGS__)
#define OHMU_LOGF(p, f, ...)                                                  \
  SC_GEN_LOGF (ohmu_package_id, SC_LC_NORMAL, (p), (f), __VA_ARGS__)

/* convenience global log macros will only print if identifier <= 0 */
#define OHMU_GLOBAL_TRACE(s) OHMU_GLOBAL_LOG (SC_LP_TRACE, (s))
#define OHMU_GLOBAL_LDEBUG(s) OHMU_GLOBAL_LOG (SC_LP_DEBUG, (s))
#define OHMU_GLOBAL_VERBOSE(s) OHMU_GLOBAL_LOG (SC_LP_VERBOSE, (s))
#define OHMU_GLOBAL_INFO(s) OHMU_GLOBAL_LOG (SC_LP_INFO, (s))
#define OHMU_GLOBAL_STATISTICS(s) OHMU_GLOBAL_LOG (SC_LP_STATISTICS, (s))
#define OHMU_GLOBAL_PRODUCTION(s) OHMU_GLOBAL_LOG (SC_LP_PRODUCTION, (s))
#define OHMU_GLOBAL_ESSENTIAL(s) OHMU_GLOBAL_LOG (SC_LP_ESSENTIAL, (s))
#define OHMU_GLOBAL_LERROR(s) OHMU_GLOBAL_LOG (SC_LP_ERROR, (s))
#define OHMU_GLOBAL_TRACEF(f, ...)                                            \
  OHMU_GLOBAL_LOGF (SC_LP_TRACE, (f), __VA_ARGS__)
#define OHMU_GLOBAL_LDEBUGF(f, ...)                                           \
  OHMU_GLOBAL_LOGF (SC_LP_DEBUG, (f), __VA_ARGS__)
#define OHMU_GLOBAL_VERBOSEF(f, ...)                                          \
  OHMU_GLOBAL_LOGF (SC_LP_VERBOSE, (f), __VA_ARGS__)
#define OHMU_GLOBAL_INFOF(f, ...)                                             \
  OHMU_GLOBAL_LOGF (SC_LP_INFO, (f), __VA_ARGS__)
#define OHMU_GLOBAL_STATISTICSF(f, ...)                                       \
  OHMU_GLOBAL_LOGF (SC_LP_STATISTICS, (f), __VA_ARGS__)
#define OHMU_GLOBAL_PRODUCTIONF(f, ...)                                       \
  OHMU_GLOBAL_LOGF (SC_LP_PRODUCTION, (f), __VA_ARGS__)
#define OHMU_GLOBAL_ESSENTIALF(f, ...)                                        \
  OHMU_GLOBAL_LOGF (SC_LP_ESSENTIAL, (f), __VA_ARGS__)
#define OHMU_GLOBAL_LERRORF(f, ...)                                           \
  OHMU_GLOBAL_LOGF (SC_LP_ERROR, (f), __VA_ARGS__)
#define OHMU_GLOBAL_NOTICE OHMU_GLOBAL_STATISTICS
#define OHMU_GLOBAL_NOTICEF OHMU_GLOBAL_STATISTICSF

/* convenience log macros that are active on every processor */
#define OHMU_TRACE(s) OHMU_LOG (SC_LP_TRACE, (s))
#define OHMU_LDEBUG(s) OHMU_LOG (SC_LP_DEBUG, (s))
#define OHMU_VERBOSE(s) OHMU_LOG (SC_LP_VERBOSE, (s))
#define OHMU_INFO(s) OHMU_LOG (SC_LP_INFO, (s))
#define OHMU_STATISTICS(s) OHMU_LOG (SC_LP_STATISTICS, (s))
#define OHMU_PRODUCTION(s) OHMU_LOG (SC_LP_PRODUCTION, (s))
#define OHMU_ESSENTIAL(s) OHMU_LOG (SC_LP_ESSENTIAL, (s))
#define OHMU_LERROR(s) OHMU_LOG (SC_LP_ERROR, (s))
#define OHMU_TRACEF(f, ...) OHMU_LOGF (SC_LP_TRACE, (f), __VA_ARGS__)
#define OHMU_LDEBUGF(f, ...) OHMU_LOGF (SC_LP_DEBUG, (f), __VA_ARGS__)
#define OHMU_VERBOSEF(f, ...) OHMU_LOGF (SC_LP_VERBOSE, (f), __VA_ARGS__)
#define OHMU_INFOF(f, ...) OHMU_LOGF (SC_LP_INFO, (f), __VA_ARGS__)
#define OHMU_STATISTICSF(f, ...) OHMU_LOGF (SC_LP_STATISTICS, (f), __VA_ARGS__)
#define OHMU_PRODUCTIONF(f, ...) OHMU_LOGF (SC_LP_PRODUCTION, (f), __VA_ARGS__)
#define OHMU_ESSENTIALF(f, ...) OHMU_LOGF (SC_LP_ESSENTIAL, (f), __VA_ARGS__)
#define OHMU_LERRORF(f, ...) OHMU_LOGF (SC_LP_ERROR, (f), __VA_ARGS__)
#define OHMU_NOTICE OHMU_STATISTICS
#define OHMU_NOTICEF OHMU_STATISTICSF

#endif /* !OHMU_BASE_H */
