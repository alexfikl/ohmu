-- SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
-- SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
--
-- SPDX-License-Identifier: CECILL-2.0

output = "init.p4est"
save_partition = 0
min_level = 5
max_level = 8
epsilon_refine = 0.1

function f(x, y, z)
    local cx = 0.2
    local cy = 0.5
    local cz = 0.5
    local rmax = 0.1

    local r = math.sqrt((x - cx)^2 + (y - cy)^2 + (z - cz)^2)
    local rho

    if (r < rmax) then
        rho = 1
    else
        rho = 0
    end


    return rho, 1, 0, 0
end
