-- SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
-- SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
--
-- SPDX-License-Identifier: CECILL-2.0

settings = {
    -- the number of refinements of the initial solution
    initial_refine = 0,

    -- advance in time using subcycling
    subcycle = false,

    -- the max time of the simulation which will run from [0, tmax] or for
    -- imax number of iterations.
    tmax = 1.14,

    -- the maximum number of iterations, if negative, we use tmax
    imax = 100,

    -- the Courant number used to compute the time step
    cfl = 1.0,

    -- the name of the plugin. the name of the dynamic library should be
    -- libohmu_plugin_name.so
    plugin_name = "gameoflife",

    -- the number of cells in a patch
    patch_size = 1,

    -- the size of the ghost layer in each patch. with the patch_size, this
    -- gives the total size of the patch: patch_size + 2 * patch_ghosts.
    patch_ghosts = 0,

    -- the minimum level of refinement of the mesh per process
    min_allowed_level = 7,

    -- the maximum level of refinement of the mesh per process
    max_allowed_level = 7,

    -- the threshold for refining
    epsilon_refine = 0.1,

    -- the threshold for coarsening
    epsilon_coarsen = 0.1,

    -- initial refinement is uniform (0, 1)
    uniform_fill = true,

    -- minimum number of starting quadrants per process
    min_quadrants = 16,

    -- the type of the connectivity. see p4est_connectivity.h
    connectivity = 'periodic',
}

gameoflife = {
    -- the location of the *.lif file that provides the initial state.
    -- an absolute path is expected
    file = "/local/home/afikl/code/p4estest/plugins/gameoflife/data/max.lif",

    -- the Game of Life requires a fixed level (no refinement being done).
    -- this options fixes that level
    level = 7
}
