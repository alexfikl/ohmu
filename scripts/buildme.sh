#!/bin/bash

# SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
# SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
#
# SPDX-License-Identifier: CECILL-2.0

# get cmdline arguments
install_prefix=${1:-/usr}
prefix_path=${2:-${install_prefix}}

# get current paths
project_path=$(pwd)
build_path="${project_path}/build"
plugins_path="${project_path}/plugins"
plugins_build_path="${project_path}/build/plugins"

# install the library
[ -d "${build_path}" ] || mkdir ${build_path}
cd ${build_path}
cmake -DCMAKE_PREFIX_PATH=${prefix_path} \
      -DCMAKE_INSTALL_PREFIX=${install_prefix} \
      -DCMAKE_MODULE_PATH="${project_path}/cmake" \
      ${project_path}
make
make install

# install all the plugins in the plugin dir
[ -d ${plugins_build_path} ] || mkdir ${plugins_build_path}
for plugin in $(ls ${plugins_path})
do
    plugin_path="${plugins_path}/${plugin}"
    plugin_build_path="${plugins_build_path}/${plugin}"

    [ -d "${plugin_build_path}" ] || mkdir ${plugin_build_path}
    cd ${plugin_build_path}
    cmake -DCMAKE_PREFIX_PATH=${prefix_path} \
          -DCMAKE_INSTALL_PREFIX=${install_prefix} \
          -DCMAKE_MODULE_PATH="${project_path}/cmake;${build_path}/cmake" \
          ${plugin_path}
    make
    make install
done
