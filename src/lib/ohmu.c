// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "ohmu.h"

/**
 * \brief Wrap an existing forest.
 *
 * An extra constructor for p4est_wrap_t. The provided constructors create a
 * new forest from provided information. Since we load our forest from
 * a file, we need a constructor that takes an existing forest as an argument.
 */
static p4est_wrap_t *
ohmu_wrap_new (p4est_t *p4est, p4est_replace_t replace_fn,
               p4est_connect_type_t btype, void *user_pointer)
{
  p4est_wrap_t *pp;

  pp = P4EST_ALLOC_ZERO (p4est_wrap_t, 1);

  pp->params.hollow = 0;
  pp->params.replace_fn = replace_fn;
  pp->params.mesh_params.btype = btype;
  pp->params.user_pointer = user_pointer;

  pp->p4est_dim = P4EST_DIM;
  pp->p4est_half = P4EST_HALF;
  pp->p4est_faces = P4EST_FACES;
  pp->p4est_children = P4EST_CHILDREN;
  pp->conn = p4est->connectivity;
  pp->p4est = p4est;

  pp->weight_exponent = 0; /* keep this event though using ALLOC_ZERO */

  if (!pp->params.hollow)
  {
    pp->flags = P4EST_ALLOC_ZERO (uint8_t, pp->p4est->local_num_quadrants);
    pp->ghost = p4est_ghost_new (pp->p4est, btype);
    pp->mesh = p4est_mesh_new_ext (pp->p4est, pp->ghost, 1, 1, btype);
  }

  pp->p4est->user_pointer = pp;

  return pp;
}

/**
 * \brief Return 1 or 0 whether the current solution should be saved.
 */
static int
ohmu_should_save (ohmu_t *ohmu)
{
  ohmu_parameters_t p = ohmu->params;

  double            t = ohmu->t;
  double            tmax = p.tmax;

  /* always write if the number of outputs is negative */
  if (p.outputs < 0)
  {
    return 1;
  }

  /* always write the last time step */
  if (OHMU_FUZZY_NULL (t - tmax))
  {
    return 1;
  }

  int    times_saved = ohmu->writer->file_index;
  double interval = p.tmax / p.outputs;

  if ((t - (times_saved - 1) * interval) > interval)
  {
    return 1;
  }

  return 0;
}

ohmu_t *
ohmu_new (sc_MPI_Comm mpicomm, ohmu_configreader_t *cfg)
{
  p4est_t               *p4est = NULL;
  p4est_connectivity_t **connectivity = NULL;
  ohmu_parameters_t     *params;

  ohmu_t                *ohmu = OHMU_ALLOC (ohmu_t, 1);

  /* read the configuration */
  ohmu_parameters_init (&ohmu->params, cfg);
  ohmu->cfg = cfg;

  /* load the plugin */
  ohmu->plugin = ohmu_plugin_load (ohmu->params.plugin_name);
  ohmu->plugin->initialize (ohmu);
  if (ohmu_plugin_check_version (ohmu->plugin))
  {
    SC_ABORT ("Plugin version is incompatible.");
  }

  /* load the initial condition */
  p4est = p4est_load_ext (ohmu->params.restart_file, mpicomm,
                          ohmu->plugin->data_size, 1, /* load user data */
                          1,    /* uniformly partition all the data */
                          0,    /* each process reads headers */
                          NULL, /* user_pointer */
                          connectivity);
  OHMU_GLOBAL_INFO ("Finished creating p4est.\n");

  /* wrap the forest */
  ohmu->pp = ohmu_wrap_new (p4est, ohmu->plugin->replace_quadrant,
                            P4EST_CONNECT_FACE, ohmu);
  OHMU_GLOBAL_INFO ("Finished creating wrapper.\n");

  /* initialize the hdf5 writer */
  ohmu->writer
      = ohmu_hdf5_new_ext (p4est, ohmu->params.plugin_name,
                           ohmu->params.mesh_scale, ohmu->params.mesh_level,
                           ohmu->params.mesh_rank, ohmu->params.mesh_treeid);

  /* initialize some of the other stuff */
  ohmu->t = 0;
  ohmu->dt = ohmu->params.tmax;
  ohmu->min_current_level = 0;
  ohmu->max_current_level = P4EST_MAXLEVEL;
  ohmu->iteration = 0;

  params = &(ohmu->params);
  OHMU_GLOBAL_PRODUCTIONF ("This is %s\n", OHMU_PACKAGE_STRING);

  OHMU_GLOBAL_PRODUCTIONF ("%-24s %g\n", "TMAX", params->tmax);
  OHMU_GLOBAL_PRODUCTIONF ("%-24s %ld\n", "MAX_IT", params->imax);
  OHMU_GLOBAL_PRODUCTIONF ("%-24s %g\n", "CFL", params->cfl);
  OHMU_GLOBAL_PRODUCTIONF ("%-24s %s\n", "CONNECTIVITY",
                           params->connectivity_name);
  OHMU_GLOBAL_PRODUCTIONF ("%-24s %s\n", "PLUGIN", params->plugin_name);

  OHMU_GLOBAL_PRODUCTIONF ("%-24s %d\n", "MIN_ALLOWED_LEVEL",
                           params->min_allowed_level);
  OHMU_GLOBAL_PRODUCTIONF ("%-24s %d\n", "MAX_ALLOWED_LEVEL",
                           params->max_allowed_level);
  OHMU_GLOBAL_PRODUCTIONF ("%-24s %g\n", "EPS_REFINE", params->epsilon_refine);
  OHMU_GLOBAL_PRODUCTIONF ("%-24s %g\n", "EPS_COARSEN",
                           params->epsilon_coarsen);

  return ohmu;
}

void
ohmu_destroy (ohmu_t *ohmu, int allow_restart)
{
  if (ohmu == NULL)
  {
    return;
  }

  /* write a main file with the time series */
  ohmu_hdf5_write_xmf_time_series (ohmu->writer);

  /* save the forest if we want to restart later on */
  if (allow_restart)
  {
    p4est_t    *p4est = ohmu_get_p4est (ohmu);
    const char *filename = ohmu->params.restart_file;

    p4est_save_ext (filename, p4est, 1, 0);
  }

  /* free everything */
  ohmu->plugin->finalize (ohmu);
  ohmu_plugin_unload (ohmu->plugin);

  ohmu_hdf5_destroy (ohmu->writer);
  p4est_wrap_destroy (ohmu->pp);

  OHMU_FREE (ohmu);
}

p4est_t *
ohmu_get_p4est (ohmu_t *ohmu)
{
  return ohmu->pp->p4est;
}

p4est_ghost_t *
ohmu_get_ghost_layer (ohmu_t *ohmu)
{
  return p4est_wrap_get_ghost (ohmu->pp);
}

p4est_mesh_t *
ohmu_get_mesh (ohmu_t *ohmu)
{
  return p4est_wrap_get_mesh (ohmu->pp);
}

ohmu_t *
ohmu_from_p4est (p4est_t *p4est)
{
  p4est_wrap_t *pp = (p4est_wrap_t *)p4est->user_pointer;

  return (ohmu_t *)pp->params.user_pointer;
}

void
ohmu_exchange_ghost_data (ohmu_t *ohmu)
{
  p4est_t       *p4est = ohmu_get_p4est (ohmu);
  p4est_ghost_t *ghosts = ohmu_get_ghost_layer (ohmu);

  size_t data_size = (ghosts->ghosts.elem_count) * (ohmu->plugin->data_size);
  void  *ghost_data = ohmu->plugin->ghost_data;

  ghost_data = sc_realloc (ohmu_package_id, ghost_data, data_size);
  p4est_ghost_exchange_data (p4est, ghosts, ghost_data);
}

int
ohmu_finished (ohmu_t *ohmu)
{
  if (ohmu->params.imax >= 0)
  {
    return ohmu->iteration > ohmu->params.imax;
  }
  else
  {
    return ohmu->t > ohmu->params.tmax;
  }
}

void
ohmu_advance (ohmu_t *ohmu)
{
  sc_MPI_Comm    mpicomm = ohmu->pp->p4est->mpicomm;
  double         tmax = ohmu->params.tmax;
  double         dt_loc = 0;
  ohmu_plugin_t *plugin = ohmu->plugin;

  /* if defined by the plugin, do any preprocessing operations */
  if (plugin->step_begin != NULL)
  {
    plugin->step_begin (ohmu);
  }

  /* compute the CFL condition / time step */
  dt_loc = ohmu->plugin->step_cfl (ohmu);

  /* make the sure dt_loc is positive and will not get bigger than tmax */
  dt_loc = (dt_loc < 0 ? tmax : dt_loc * ohmu->params.cfl);
  dt_loc = fmin (dt_loc, fabs (tmax - ohmu->t));

  /* take the min over all processes */
  sc_MPI_Allreduce (&dt_loc, &(ohmu->dt), 1, sc_MPI_DOUBLE, sc_MPI_MIN,
                    mpicomm);

  /* and call the plugin function to advance in time */
  plugin->step_advance (ohmu);

  /* if defined by the plugin, do any postprocessing operations */
  if (plugin->step_end != NULL)
  {
    plugin->step_end (ohmu);
  }

  /* incremenent */
  ++ohmu->iteration;
  ohmu->t += ohmu->dt;

  OHMU_GLOBAL_ESSENTIALF ("[%ld] t = %-10g\t dt = %.8g %s\n", ohmu->iteration,
                          ohmu->t, ohmu->dt,
                          ohmu_should_save (ohmu) ? "***" : "");
}

void
ohmu_adapt (ohmu_t *ohmu)
{
  int changed = 0;

  /* if the min and max allowed levels are the same, there's nothing to do */
  if (ohmu->params.min_allowed_level == ohmu->params.max_allowed_level)
  {
    return;
  }

  /* mark quadrants for refinement / coarsening */
  changed = ohmu->plugin->step_mark (ohmu);

  /* nothing marked => nothing to adapt */
  if (!changed)
  {
    return;
  }

  /* adapt the forest */
  changed = p4est_wrap_adapt (ohmu->pp);

  /* if needed, partition */
  if (changed)
  {
    changed = p4est_wrap_partition (ohmu->pp, 0, NULL, NULL, NULL);
  }

  /* complete the cycle */
  if (changed)
  {
    p4est_wrap_complete (ohmu->pp);
  }
}

void
ohmu_save (ohmu_t *ohmu)
{
  if (ohmu->plugin->step_save == NULL)
  {
    return;
  }

  if (ohmu_should_save (ohmu) == 0)
  {
    return;
  }

  ohmu->plugin->step_save (ohmu);
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
