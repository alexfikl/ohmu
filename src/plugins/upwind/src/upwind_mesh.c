// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "upwind.h"
#include "upwind_bits.h"

#include <p4est_bits.h>

static void
upwind_update_face_ghosts (ohmu_solver_t *solver, upwind_data_t *patch,
                           upwind_data_t *npatch, uint8_t face)
{
  ohmu_solver_settings_t settings = solver->settings;
  uint32_t               patch_size = settings.patch_size;
  uint32_t               patch_ghosts = settings.patch_ghosts;
  uint32_t               row_size = patch_size + 2 * patch_ghosts;
  uint32_t               slice_size = row_size * row_size;

  uint32_t               xi = 0;
  uint32_t               yi = 0;
  uint32_t               zi = 0;
  uint32_t               gid = 0;
  uint32_t               nid = 0;

  upwind_loop_t          d[3];
  upwind_ghost_loop (patch_size, patch_ghosts, face, d);

  // iterate over all the ghost cells on the given face
#ifdef P4_TO_P8
  for (zi = d[2].start; zi < d[2].end; ++zi)
  {
#endif
    for (yi = d[1].start; yi < d[1].end; ++yi)
    {
      for (xi = d[0].start; xi < d[0].end; ++xi)
      {
        // compute the id of the ghost cell
        gid = slice_size * zi + row_size * yi + xi;

        // compute the id of the corresponding neighboring cell
        nid = slice_size * (zi + d[2].stride) + row_size * (yi + d[1].stride)
              + (xi + d[0].stride);

        // copy the values
        upwind_patch_cell_copy (&patch[gid], &npatch[nid]);
      }
    }
#ifdef P4_TO_P8
  }
#endif
}

static void
upwind_update_ghosts_equal (ohmu_solver_t *solver, p4est_quadrant_t *q,
                            p4est_iter_face_side_t side, uint8_t face)
{
  p4est_quadrant_t *qs = side.is.full.quad;

  upwind_data_t    *patch = upwind_quadrant_data (q);
  upwind_data_t    *npatch = NULL;

  // get the neighbor's patch
  if (side.is.full.is_ghost)
  {
    npatch = (upwind_data_t *)ohmu_ghost_data_array_index (
        solver, side.is.full.quadid);
  }
  else
  {
    npatch = upwind_quadrant_data (qs);
  }

  upwind_update_face_ghosts (solver, patch, npatch, face);
}

static void
upwind_update_ghosts_bigger (ohmu_solver_t *solver, p4est_quadrant_t *q,
                             p4est_iter_face_side_t side, int face)
{
  p4est_t          *p4est = ohmu_solver_p4est (solver);
  p4est_quadrant_t *qs = side.is.full.quad;

  upwind_data_t    *patch = upwind_quadrant_data (q);
  upwind_data_t    *npatch = NULL;
  upwind_data_t    *cpatch
      = (upwind_data_t *)sc_mempool_alloc (p4est->user_data_pool);

  uint8_t child = 0;

  // get the neighbor's patch
  if (side.is.full.is_ghost)
  {
    npatch = ohmu_ghost_data_array_index (solver, side.is.full.quadid);
  }
  else
  {
    npatch = upwind_quadrant_data (qs);
  }

  // find the child of the neighbor that shares the face with our
  // given quadrant.
  // TODO: better way to do this?
  child |= (q->x > qs->x);
  child |= (q->y > qs->y) << 1;
#ifdef P4_TO_P8
  child |= (q->z > qs->z) << 2;
#endif

  // construct the data for this virtual child quadrant
  upwind_patch_child_copy (solver, cpatch, npatch, child);

  // update the ghost layer for the current patch
  upwind_update_face_ghosts (solver, patch, cpatch, face);

  // free the virtual child patch
  sc_mempool_free (p4est->user_data_pool, cpatch);
}

static void
upwind_update_ghosts_smaller (ohmu_solver_t *solver, p4est_quadrant_t *q,
                              p4est_iter_face_side_t side, int face)
{
  p4est_t          *p4est = ohmu_solver_p4est (solver);

  p4est_quadrant_t *qs = NULL;
  uint8_t           child = 0;

  upwind_data_t    *patch = upwind_quadrant_data (q);
  upwind_data_t    *npatch = NULL;
  upwind_data_t    *ppatch
      = (upwind_data_t *)sc_mempool_alloc (p4est->user_data_pool);

  for (uint8_t i = 0; i < P4EST_HALF; ++i)
  {
    qs = side.is.hanging.quad[i];

    // get the neighbor's patch
    if (side.is.hanging.is_ghost[i])
    {
      npatch = ohmu_ghost_data_array_index (solver, side.is.hanging.quadid[i]);
    }
    else
    {
      npatch = upwind_quadrant_data (qs);
    }

    // find the child id of the current neighbor in its parent
    child = p4est_quadrant_child_id (qs);

    // construct the data to put into the virtual parent
    upwind_patch_parent_copy (solver, ppatch, npatch, child);
  }

  // update the ghost layer of the current patch
  upwind_update_face_ghosts (solver, patch, ppatch, face);

  // free the virtual parent patch
  sc_mempool_free (p4est->user_data_pool, ppatch);
}

void
upwind_exchange_ghosts (ohmu_solver_t *solver, p4est_quadrant_t *q,
                        p4est_topidx_t tree, p4est_locidx_t qid)
{
  p4est_t               *p4est = ohmu_solver_p4est (solver);
  p4est_ghost_t         *ghost_layer = ohmu_solver_ghost (solver);
  p4est_mesh_t          *mesh = ohmu_solver_mesh (solver);

  p4est_iter_face_side_t side;

  for (int face = 0; face < P4EST_FACES; ++face)
  {
    side = ohmu_quadrant_face_neighbor (p4est, ghost_layer, mesh, tree, qid,
                                        face);

    if (side.is_hanging)
    {
      upwind_update_ghosts_smaller (solver, q, side, face);
    }
    else if (side.is.full.quad->level > q->level)
    {
      upwind_update_ghosts_bigger (solver, q, side, face);
    }
    else
    {
      upwind_update_ghosts_equal (solver, q, side, face);
    }
  }
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
