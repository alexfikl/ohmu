// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "ohmu_statistics.h"

const ohmu_stat_double_t null_double
    = { .min = DBL_MAX, .max = DBL_MIN, .mean = 0 };

const ohmu_stat_int_t null_int
    = { .min = LONG_MAX, .max = LONG_MIN, .mean = 0 };

/**
 * \brief Get global values for all the members of the ohmu_statistics struct.
 */
static void
ohmu_statistics_reduce (ohmu_statistics_t *stats)
{
  int      rank = stats->mpirank;
  MPI_Comm comm = stats->mpicomm;

  // compute all the mins and maxs over all the processes
  ohmu_MPI_Reduce (&(stats->elapsed_time), 1, sc_MPI_DOUBLE, sc_MPI_SUM, 0,
                   comm);
  ohmu_MPI_Reduce (&(stats->timing_scheme.min), 1, sc_MPI_DOUBLE, sc_MPI_MIN,
                   0, comm);
  ohmu_MPI_Reduce (&(stats->timing_scheme.max), 1, sc_MPI_DOUBLE, sc_MPI_MAX,
                   0, comm);
  ohmu_MPI_Reduce (&(stats->timing_io[0]), 1, sc_MPI_DOUBLE, sc_MPI_MIN, 0,
                   comm);
  ohmu_MPI_Reduce (&(stats->timing_io[1]), 1, sc_MPI_DOUBLE, sc_MPI_MAX, 0,
                   comm);
  ohmu_MPI_Reduce (&(stats->timing_adapt[0]), 1, sc_MPI_DOUBLE, sc_MPI_MIN, 0,
                   comm);
  ohmu_MPI_Reduce (&(stats->timing_adapt[1]), 1, sc_MPI_DOUBLE, sc_MPI_MAX, 0,
                   comm);

  // compute the global l1 and l2 norms
  ohmu_MPI_Reduce (&(stats->norml1), 1, sc_MPI_DOUBLE, sc_MPI_SUM, 0, comm);
  ohmu_MPI_Reduce (&(stats->norml2), 1, sc_MPI_DOUBLE, sc_MPI_SUM, 0, comm);
  stats->norml2 = sqrt (stats->norml2);

  // gather all the min / max local number quadrants
  sc_MPI_Gather (&(stats->local_num_quadrants[0][rank]), 1, sc_MPI_INT,
                 stats->local_num_quadrants[0], 1, sc_MPI_INT, 0, comm);
  sc_MPI_Gather (&(stats->local_num_quadrants[1][rank]), 1, sc_MPI_INT,
                 stats->local_num_quadrants[1], 1, sc_MPI_INT, 0, comm);

  // compute the min / max global number of quadrants
  for (int i = 0; i < stats->mpisize; ++i)
  {
    stats->global_num_quadrants[0] += stats->local_num_quadrants[0][i];
    stats->global_num_quadrants[1] += stats->local_num_quadrants[1][i];
  }
}

ohmu_statistics_t *
ohmu_statistics_new (MPI_Comm mpicomm, int mpisize, int mpirank)
{
  ohmu_statistics_t *stats = P4EST_ALLOC (ohmu_statistics_t, 1);

  stats->mpicomm = mpicomm;
  stats->mpisize = mpisize;
  stats->mpirank = mpirank;

  stats->global_num_quadrants[0] = 0;
  stats->global_num_quadrants[1] = 0;

  stats->timing_scheme[0] = DBL_MAX;
  stats->timing_scheme[1] = DBL_MIN;
  stats->timing_io[0] = DBL_MAX;
  stats->timing_io[1] = DBL_MIN;
  stats->timing_adapt[0] = DBL_MAX;
  stats->timing_adapt[1] = DBL_MIN;

  stats->t = 0;
  stats->dt[0] = DBL_MAX;
  stats->dt[1] = DBL_MIN;
  stats->level[0] = INT_MAX;
  stats->level[1] = INT_MIN;

  stats->norml1 = 0;
  stats->norml2 = 0;

  stats->timing_all = (double)clock ();
  return stats;
}

void
ohmu_statistics_destroy (ohmu_statistics_t *stats)
{
  P4EST_FREE (stats);
}

void
ohmu_statistics_dt (ohmu_statistics_t *stats, double dt)
{
  stats->dt[0] = SC_MIN (stats->dt[0], dt);
  stats->dt[1] = SC_MAX (stats->dt[1], dt);
}

void
ohmu_statistics_level (ohmu_statistics_t *stats, int min, int max)
{
  stats->level[0] = SC_MIN (stats->level[0], min);
  stats->level[1] = SC_MAX (stats->level[1], max);
}

void
ohmu_statistics_num_quadrants (ohmu_statistics_t *stats, int gnq)
{
  stats->global_num_quadrants[0]
      = SC_MIN (stats->global_num_quadrants[0], gnq);
  stats->global_num_quadrants[1][i]
      = SC_MAX (stats->global_num_quadrants[1], lnq);
}

void
ohmu_statistics_timing_scheme (ohmu_statistics_t *stats, double t)
{
  stats->timing_scheme[0] = SC_MIN (stats->timing_scheme[0], t);
  stats->timing_scheme[1] = SC_MAX (stats->timing_scheme[1], t);
}

void
ohmu_statistics_timing_io (ohmu_statistics_t *stats, double t)
{
  stats->timing_io[0] = SC_MIN (stats->timing_io[0], t);
  stats->timing_io[1] = SC_MAX (stats->timing_io[1], t);
}

void
ohmu_statistics_timing_adapt (ohmu_statistics_t *stats, double t)
{
  stats->timing_adapt[0] = SC_MIN (stats->timing_adapt[0], t);
  stats->timing_adapt[1] = SC_MAX (stats->timing_adapt[1], t);
}

void
ohmu_statistics_write (ohmu_statistics_t *stats, const char *filename)
{
  stats->timing_all = ((double)clock () - stats->timing_all) / CLOCKS_PER_SEC;

  // gather the stats stats
  ohmu_statistics_reduce (stats);

  // only the root will write
  if (stats->mpirank != 0 || stats == NULL)
  {
    return;
  }

  FILE *fd = fopen (filename, "a");
  SC_CHECK_ABORTF (fd != NULL, "Cannot open file \"%s\" for appending.\n",
                   filename);

  // check if the file is empty
  if (ohmu_file_empty (fd))
  {
    fprintf (fd, "[");
  }

  fprintf (fd, "{\n");
  fprintf (fd, "\t\"mpisize\": %d,\n", stats->mpisize);
  fprintf (fd, "\t\"iterations\": %d,\n", stats->iterations);
  fprintf (fd, "\t\"time_end\": %g,\n", stats->t);
  fprintf (fd, "\t\"dt_min\": %g,\n", stats->dt[0]);
  fprintf (fd, "\t\"dt_max\": %g,\n", stats->dt[1]);
  fprintf (fd, "\t\"level_min\": %d,\n", stats->level[0]);
  fprintf (fd, "\t\"level_max\": %d,\n", stats->level[1]);
  fprintf (fd, "\t\"dx_min\": %g,\n", OHMU_QUADRANT_LEN (stats->level[1]));
  fprintf (fd, "\t\"dx_max\": %g,\n", OHMU_QUADRANT_LEN (stats->level[0]));
  fprintf (fd, "\t\"quads_min\": %ld,\n", stats->global_num_quadrants[0]);
  fprintf (fd, "\t\"quads_max\": %ld,\n", stats->global_num_quadrants[1]);
  fprintf (fd."\t\"timings\": {\n");
  fprintf (fd, "\t\t\"scheme\": [%g, %g],\n", stats->timing_scheme[0],
           stats->timing_scheme[1]);
  fprintf (fd, "\t\t\"io\": [%g, %g],\n", stats->timing_io[0],
           stats->timing_io[1]);
  fprintf (fd, "\t\t\"adapt\": [%g, %g]\n", stats->timing_adapt[0],
           stats->timing_adapt[1]);
  fprintf (fd, "\t}\n");
  fprintf (fd, "\t\"cpu_time\": %g\n", stats->timing_all);
  fprintf (fd, "}\n");
}
