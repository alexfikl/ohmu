// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#ifndef OHMU_H
#define OHMU_H

#include <ohmu_hdf5.h>
#include <ohmu_parameters.h>

#include <ohmu_plugins.h>

#ifndef P4_TO_P8
#include <p4est_wrap.h>
#else
#include <p8est_wrap.h>
#endif

/**
 * \brief Main structure that defines a simulation.
 *
 * This structure encapsulates all the p4est data structures and some extra
 * information that defines a simulation.
 */
struct ohmu
{
  /* main structures */
  ohmu_parameters_t    params; /**< Parameters of the current simulation. */
  ohmu_configreader_t *cfg;    /**< Not owned. */
  p4est_wrap_t        *pp;     /**< p4est wrapper. */
  ohmu_hdf5_t         *writer; /**< HDF5 writer */
  ohmu_plugin_t       *plugin; /**< An instance of the loaded plugin */

  /* Information about the current iteration. */
  int64_t iteration;         /* The current iteration */
  double  t;                 /**< The time at the current iteration. */
  double  dt;                /**< The time step. */
  int     min_current_level; /**< Min level in the forest */
  int     max_current_level; /**< Max level in the forest */
};

/**
 * \brief Create a new main structure.
 *
 * This is the main structure of this library. It is meant to contain
 * all the information about a running simulation. This includes:
 *  * the current parameters and configuration
 *  * the current loaded plugin / simulation
 *  * some other info like the time t, the time step, the iteration count, etc.s
 *
 * \param [in] mpicomm  The main MPI communicator.
 * \param [in] cfg      An existing configuration reader. We read the
 *                      configuration options and set them in the
 *                      ohmu_parameters_t struct. The reader is saved in the
 *                      struct and can be further used by the plugin to read
 *                      options from the same file.
 */
ohmu_t *ohmu_new (MPI_Comm mpicomm, ohmu_configreader_t *cfg);

/**
 * \brief Destroy the struct.
 *
 * Frees all the structures inside except the configuration reader.
 *
 * \param[in] allow_restart Boolean to mark that the current state of the
 *                          forest should be saved for a later restart.
 */
void ohmu_destroy (ohmu_t *ohmu, int allow_restart);

/**
 * \brief Get the p4est structure.
 *
 * The p4est struct is wrapped inside p4est_wrap_t.
 */
p4est_t *ohmu_get_p4est (ohmu_t *ohmu);

/**
 * \brief Get the current ghost layer.
 *
 * The ghost layer is wrapped inside p4est_wrap_t.
 */
p4est_ghost_t *ohmu_get_ghost_layer (ohmu_t *ohmu);

/**
 * \brief Get the current mesh.
 *
 * The mesh is wrapped inside p4est_wrap_t.
 */
p4est_mesh_t *ohmu_get_mesh (ohmu_t *ohmu);

/**
 * \brief Get the main structure from inside the p4est user_pointer.
 *
 * Since we use p4est_wrap_t, our struct can be found in its user pointer,
 * which is itself in p4est's user pointer.
 */
ohmu_t *ohmu_from_p4est (p4est_t *p4est);

/**
 * \brief Exchange ghost data.
 *
 * The ghost data is held by the plugin and it decides what and when it should
 * be updated. This is a simple helper function for the most simple case:
 * the ghost data is contained inside each quadrant and we want to exchange
 * all of it.
 *
 * This is done simply by allocating a new ghost data array and calling
 * p4est_ghost_exchange_data.
 */
void ohmu_exchange_ghost_data (ohmu_t *ohmu);

/**
 * \brief Verify the stopping condition for the simulation.
 *
 * This function uses params.tmax and params.imax to decide when the
 * loop is over in the following way:
 *      if (imax < 0) {
 *          return t > tmax;
 *      } else {
 *          return iteration > imax;
 *      }
 */
int ohmu_finished (ohmu_t *ohmu);

/**
 * \brief Advance the solution in time by one time step.
 */
void ohmu_advance (ohmu_t *ohmu);

/**
 * \brief Adapt, balance and partition the forest.
 *
 * In this phase we mark the quadrands for refining and coarsening using the
 * API provided by p4est_wrap_t. If no quadrants were marked, nothing happens.
 * The marking is left to the plugin.
 *
 * Once the marking is done, we proceed to call p4est_wrap_adapt,
 * p4est_wrap_partition and p4est_wrap_complete only if the step before
 * changed the forest.
 */
void ohmu_adapt (ohmu_t *ohmu);

/**
 * \brief Write the current solution to a file.
 *
 * The file is given by:
 *          (plugin_name)_(# of saved file).h5 and .xmf
 *
 * If the plugin provided a save function and the current step should be saved,
 * we call the respective function. We decide whether a certain step should be
 * saved based on params.outputs (the total number of output files we should
 * issue) and the current time.
 */
void ohmu_save (ohmu_t *ohmu);

#endif /* !OHMU_H */
