// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "upwind.h"
#include "upwind_bits.h"

#include <ohmu_patch.h>

static void
upwind_scalar_init (upwind_data_t *cell, double x, double y, double z)
{
  UNUSED (z);

  double radius_sqr = SC_SQR (0.2);
  double c[3] = { 0.5, 0.5, 0.5 };

  double sqr = SC_SQR (x - c[0]) +
#ifdef P4_TO_P8
               SC_SQR (z - c[2]) +
#endif
               SC_SQR (y - c[1]);

  cell->c = cell->cnext = (sqr < radius_sqr ? 1 : 0);
}

static void
upwind_velocity_init (upwind_data_t *cell, double x, double y, double z)
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  cell->velocity[0] = 1.0;
  cell->velocity[1] = 0.0;
#ifdef P4_TO_P8
  cell->velocity[2] = 0.0;
#endif
}

void
upwind_initialize (ohmu_solver_t *solver)
{
  solver->plugin->data_size = sizeof (upwind_data_t);

  OHMU_REGISTER_PLUGIN (solver->plugin);
}

void
upwind_finalize (ohmu_solver_t *solver)
{
  UNUSED (solver);
}

void
upwind_initialize_quadrant (p4est_t *p4est, p4est_locidx_t treeid,
                            p4est_quadrant_t *q)
{
  UNUSED (treeid);

  ohmu_solver_t         *solver = (ohmu_solver_t *)p4est->user_pointer;
  ohmu_solver_settings_t settings = solver->settings;
  upwind_data_t         *patch = upwind_quadrant_data (q);

  uint32_t               patch_size = settings.patch_size;
  uint32_t               patch_ghosts = settings.patch_ghosts;
  double                 h = ohmu_quadrant_length (q) / patch_size;

  double                 v[3] = { 0, 0, 0 }; // coords of the first cell
  double                 x[3] = { 0, 0, 0 }; // coords of the current cell
  uint32_t               xi = 0;
  uint32_t               yi = 0;
  uint32_t               zi = 0;
  uint32_t               cellid = 0;

  // init the patch to 0
  memset (patch, 0, settings.num_patch_cells * sizeof (upwind_data_t));

  // compute the center of the first cell in the patch
  p4est_qcoord_to_vertex (p4est->connectivity, treeid, q->x, q->y,
#ifdef P4_TO_P8
                          q->z,
#endif
                          v);
  v[0] += 0.5 * h;
  v[1] += 0.5 * h;
#ifdef P4_TO_P8
  v[2] += 0.5 * h;

  ohmu_quadrant_print (SC_LP_INFO, q);

  for (zi = 0; zi < patch_size; ++zi)
  {
#endif
    for (yi = 0; yi < patch_size; ++yi)
    {
      for (xi = 0; xi < patch_size; ++xi)
      {
        // get the cellid
        cellid = ohmu_patch_linear_id (xi, yi, zi, patch_size, patch_ghosts);

        // offset the center to the current patch cell
        x[0] = v[0] + xi * h;
        x[1] = v[1] + yi * h;
#ifdef P4_TO_P8
        x[2] = v[2] + zi * h;
#endif
        // compute the values
        upwind_scalar_init (patch + cellid, x[0], x[1], x[2]);
        upwind_velocity_init (patch + cellid, x[0], x[1], x[2]);
      }
    }
#ifdef P4_TO_P8
  }
#endif
}

void
upwind_begin_step (ohmu_solver_t *solver, p4est_quadrant_t *q)
{
  upwind_data_t *patch = upwind_quadrant_data (q);

  for (size_t i = 0; i < solver->settings.num_patch_cells; ++i)
  {
    patch[i].cnext = patch[i].c;
  }
}

void
upwind_end_step (ohmu_solver_t *solver, p4est_quadrant_t *q)
{
  upwind_data_t *patch = upwind_quadrant_data (q);

  for (size_t i = 0; i < solver->settings.num_patch_cells; ++i)
  {
    patch[i].c = patch[i].cnext;
  }
}

void
upwind_save (ohmu_solver_t *solver, const char *filename)
{
  OHMU_GLOBAL_INFOF ("plugin: saving to file \"%s\"\n", filename);
  p4est_t               *p4est = ohmu_solver_p4est (solver);
  ohmu_solver_settings_t settings = solver->settings;
  ohmu_hdf5_writer_t    *w = solver->writer;
  ohmu_patch_iterator_t  it
      = ohmu_iterator_new (settings.patch_size, settings.patch_ghosts);

  p4est_tree_t     *tree = NULL;
  sc_array_t       *quadrants = NULL;
  p4est_quadrant_t *quadrant = NULL;
  upwind_data_t    *patch = NULL;
  double           *quad_data = NULL;

  p4est_locidx_t    local_num_quads
      = settings.num_patch_cells * p4est->local_num_quadrants;
  p4est_topidx_t jt = 0;
  int            k = 0;

  quad_data = OHMU_ALLOC (double, local_num_quads);
  for (jt = p4est->first_local_tree; jt <= p4est->last_local_tree; ++jt)
  {
    tree = p4est_tree_array_index (p4est->trees, jt);
    quadrants = &tree->quadrants;

    for (size_t i = 0; i < quadrants->elem_count; ++i)
    {
      quadrant = p4est_quadrant_array_index (quadrants, i);
      patch = upwind_quadrant_data (quadrant);

      // get all the real cell values
      ohmu_iterator_reset (&it);
      while (it.current < it.last)
      {
        quad_data[k++] = patch[it.current].c;
        ohmu_iterator_next (&it);
      }
    }
  }

  // open the new file and write our stuff
  ohmu_hdf5_writer_open (w, filename);

  ohmu_write_header (w, solver->t);
  ohmu_write_attribute (w, "cj", quad_data, 0, OHMU_HDF5_CELL_SCALAR,
                        H5T_NATIVE_DOUBLE);
  ohmu_write_footer (w);

  ohmu_hdf5_writer_close (w);

  OHMU_FREE (quad_data);
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
