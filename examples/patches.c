// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef P4_TO_P8
#define P4EST_CHILDREN 8
#define P4EST_DIM 3
#else
#define P4EST_CHILDREN 4
#define P4EST_DIM 2
#endif

#ifndef P4EST_SLICE
#define P4EST_SLICE 0
#endif

#define BIT(x, k) (((x) & (1 << (k))) >> (k))
#define SC_SQR(x) ((x) * (x))

void
print_patch (double *ids, uint32_t nc, uint32_t ng)
{
  uint32_t size = nc + 2 * ng;
  uint32_t k = 0;

  for (uint32_t y = size - 1; y < size; --y)
  {
    for (uint32_t x = 0; x < size; ++x)
    {
      k = size * y + x;
#ifdef P4_TO_P8
      k += size * size * (ng + P4EST_SLICE);
#endif
      if (ng <= x && x < (nc + ng) && ng <= y && y < (nc + ng))
      {
        printf ("\033[32;1m%4g\033[0m ", ids[k]);
      }
      else
      {
        printf ("%4g ", ids[k]);
      }
    }
    printf ("\n");
  }
  printf ("\n");
}

void
replace_cells (int num_outgoing, double *cellout[], int num_incoming,
               double *cellin[])
{
  if (num_outgoing == 1)
  { // refining
    //         printf ("refining\n");
    for (int i = 0; i < P4EST_CHILDREN; ++i)
    {
      *(cellin[i]) = *(cellout[0]);
    }
  }
  else
  {
    //         printf ("coarsening\n");
    double mean = 0;
    for (int i = 0; i < P4EST_CHILDREN; ++i)
    {
      mean += *(cellout[i]);
    }
    *(cellin[0]) = mean / P4EST_CHILDREN;
  }
}

uint32_t
cell_index (uint32_t x, uint32_t y, uint32_t z, uint32_t size[3])
{
  return size[0] * x + size[1] * y +
#ifdef P4_TO_P8
         size[2] * z +
#endif
         0;
}

void
test_coarsen (uint32_t nc, uint32_t ng)
{
  uint32_t  half = nc / 2;
  uint32_t  size[3] = { 0, 0, 0 };
  uint32_t  num_cells;

  uint32_t  xi = 0, yi = 0, zi = 0;
  uint32_t  cx = 0, cy = 0, cz = 0;
  uint32_t  ci = 0, cj = 0, ck = 0;
  uint32_t  qchild, cellid;
  uint32_t  ghost_offset = 0;

  double   *quadin[P4EST_CHILDREN];
  double   *quadout[P4EST_CHILDREN];
  double   *cellin[P4EST_CHILDREN];
  double   *cellout[P4EST_CHILDREN];

  uint32_t  n = pow (nc + 2 * ng, P4EST_DIM);
  uint32_t *ids = (uint32_t *)calloc (n, sizeof (uint32_t));

  // compute the size of the patch
  size[0] = 1;
  size[1] = nc + 2 * ng;
#ifdef P4_TO_P8
  size[2] = size[1] * size[1];
  ghost_offset = size[2] * ng;
#endif
  // compute the offset for the ghost cells
  ghost_offset += ng * (size[1] + 1);
  num_cells = pow (size[1], P4EST_DIM);

  quadout[0] = calloc (num_cells, sizeof (double));
  quadout[1] = calloc (num_cells, sizeof (double));

  for (int i = 0; i < P4EST_CHILDREN; ++i)
  {
    quadin[i] = calloc (num_cells, sizeof (double));
  }

#ifdef P4_TO_P8
  for (zi = 0; zi < nc; ++zi)
  {
#endif
    for (yi = 0; yi < nc; ++yi)
    {
      for (xi = 0; xi < nc; ++xi)
      {
        cellid = cell_index (xi, yi, zi, size) + ghost_offset;
        qchild = 4 * (zi >= half) + 2 * (yi >= half) + (xi >= half);

        quadout[0][cellid] = qchild;
      }
    }
#ifdef P4_TO_P8
  }
#endif

  for (int i = 0; i < num_cells; ++i)
  {
    quadout[1][i] = i;
  }

  print_patch (quadout[1], nc, ng);
  print_patch (quadout[0], nc, ng);

#ifdef P4_TO_P8
  for (zi = 0; zi < nc; ++zi)
  {
#endif
    for (yi = 0; yi < nc; ++yi)
    {
      for (xi = 0; xi < nc; ++xi)
      {
        // get the current cell
        cellid = cell_index (xi, yi, zi, size) + ghost_offset;
        cellout[0] = &(quadout[0][cellid]);
        printf ("idin %u\n", cellid);

        for (int child = 0; child < P4EST_CHILDREN; ++child)
        {
          ci = 2 * xi + BIT (child, 0);
          cj = 2 * yi + BIT (child, 1);
          ck = 2 * zi + BIT (child, 2);

          qchild = 4 * (ck >= nc) + 2 * (cj >= nc) + (ci >= nc);
          cellid = cell_index (ci % nc, cj % nc, ck % nc, size) + ghost_offset;

          printf ("idout %u child %u | qchild %d | (ci %u cj %u ck %u)\n",
                  cellid, child, qchild, ci, cj, ck);
          cellin[child] = &(quadin[qchild][cellid]);
        }
        replace_cells (1, cellout, P4EST_CHILDREN, cellin);
      }
    }
#ifdef P4_TO_P8
  }
#endif

  printf ("child 0\n");
  print_patch (quadin[0], nc, ng);
  printf ("child 1\n");
  print_patch (quadin[1], nc, ng);
  printf ("child 2\n");
  print_patch (quadin[2], nc, ng);
  printf ("child 3\n");
  print_patch (quadin[3], nc, ng);
}

int
main (int argc, char **argv)
{
  uint32_t nc = (uint32_t)atoi (argv[1]);
  uint32_t ng = (uint32_t)atoi (argv[2]);

  test_coarsen (nc, ng);

  return 0;
}
