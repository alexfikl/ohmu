// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include "ohmu_configreader.h"

#include <lauxlib.h>
#include <lualib.h>
#include <stdio.h>

/**
 * \brief Get the value from an expression containing a string.
 *
 * \param [in] L      The Lua state.
 * \param [in] expr   The expression to evaluate.
 *
 * \return A fully allocated string containing the value held or returned by
 *         expression. The result can be NULL if the expression does not
 *         return a string.
 *
 * \sa luaL_dostring, lua_isstring, lua_tolstring.
 */
static char *
ohmu_lua_stringexpr (lua_State *L, const char *expr)
{
  size_t      length = 0;
  char       *r = NULL;

  const char *tmp = NULL;
  char        buf[BUFSIZ];

  snprintf (buf, BUFSIZ, "evalExpr=%s", expr);
  if (!luaL_dostring (L, buf))
  {
    /* Get the value of the global variable */
    lua_getglobal (L, "evalExpr");
    if (lua_isstring (L, -1))
    {
      /* lua docs say there is no guarantee r will be valid after
       * removing it from the stack (lua_pop), so we copy it to
       * another string, just in case
       */
      tmp = lua_tolstring (L, -1, &length);
      r = SC_STRDUP (tmp);
    }
    else
    {
      SC_GLOBAL_INFOF ("Key '%s' does not contain a string.\n", expr);
    }

    lua_pop (L, 1);
  }

  return r;
}

/**
 * \brief Get the value from an expression containing a number.
 *
 * \param [in] L      The lua state.
 * \param [in] expr   The expression to evaluate.
 * \param [out] r     The resulting double.
 *
 * \return 1 if the expression returns a valid number, 0 otherwise.
 */
static int
ohmu_lua_numberexpr (lua_State *L, const char *expr, double *r)
{
  int  retval = 0;
  char buf[BUFSIZ];

  snprintf (buf, BUFSIZ, "evalExpr=%s", expr);
  if (!luaL_dostring (L, buf))
  {
    lua_getglobal (L, "evalExpr");

    if (lua_isnumber (L, -1))
    {
      *r = lua_tonumber (L, -1);
      retval = 1;
    }
    else
    {
      SC_GLOBAL_INFOF ("Key '%s' does not contain a number.\n", expr);
    }
    lua_pop (L, 1);
  }

  return retval;
}

/**
 * \brief Get the value from an expression containing an integer.
 *
 * \param [in] L      The lua state.
 * \param [in] expr   The expression to evaluate.
 * \param [out] r     The resulting integer.
 *
 * \return 1 if the expression returns a valid number, 0 otherwise.
 */
static int
ohmu_lua_intexpr (lua_State *L, const char *expr, int *r)
{
  double value = 0.0;

  if (ohmu_lua_numberexpr (L, expr, &value))
  {
    *r = (int)value;
    return 1;
  }

  return 0;
}

ohmu_configreader_t *
ohmu_configreader_new (const char *filename)
{
  ohmu_configreader_t *cfg = SC_ALLOC (ohmu_configreader_t, 1);

  cfg->L = NULL;
  cfg->strings = NULL;

  if (filename == NULL)
  {
    SC_GLOBAL_INFO ("No configuration file provided.\n");
    return cfg;
  }

  cfg->L = luaL_newstate ();
  if (cfg->L == NULL || luaL_dofile (cfg->L, filename))
  {
    SC_GLOBAL_INFO ("Invalid configuration file provided.\n");
    cfg->L = NULL;

    return cfg;
  }

  // enable stdlib for e.g. math usage
  luaL_openlibs (cfg->L);

  // TODO: do we really need to do book-keeping for the strings?
  cfg->strings = sc_list_new (NULL);

  return cfg;
}

void
ohmu_configreader_destroy (ohmu_configreader_t *cfg)
{
  if (cfg->L != NULL)
  {
    lua_close (cfg->L);

    // free all the strings we have allocated
    while (cfg->strings->first)
    {
      SC_FREE (sc_list_pop (cfg->strings));
    }

    sc_list_destroy (cfg->strings);
  }

  SC_FREE (cfg);
}

int
ohmu_config_read_int (ohmu_configreader_t *cfg, const char *key, int preset)
{
  if (cfg->L == NULL)
  {
    return preset;
  }

  int value = 0;
  int retval = ohmu_lua_intexpr (cfg->L, key, &value);

  return (retval ? value : preset);
}

double
ohmu_config_read_double (ohmu_configreader_t *cfg, const char *key,
                         double preset)
{
  if (cfg->L == NULL)
  {
    return preset;
  }

  double value = 0.0;
  int    retval = ohmu_lua_numberexpr (cfg->L, key, &value);

  return (retval ? value : preset);
}

const char *
ohmu_config_read_string (ohmu_configreader_t *cfg, const char *key,
                         const char *preset)
{
  if (cfg->L == NULL)
  {
    return preset;
  }

  const char *r = ohmu_lua_stringexpr (cfg->L, key);

  sc_list_append (cfg->strings, (void *)r);
  return (r ? r : preset);
}

void
ohmu_config_evaluate (ohmu_configreader_t *cfg, const char *funcname,
                      size_t ninps, double *x, size_t nouts, double *fx)
{
  if (!cfg || !cfg->L)
  {
    return;
  }

  lua_State *L = cfg->L;

  lua_getglobal (cfg->L, funcname);
  for (size_t i = 0; i < ninps; ++i)
  {
    lua_pushnumber (cfg->L, x[i]);
  }

  if (lua_pcall (cfg->L, ninps, nouts, 0))
  {
    SC_ABORTF ("Error executing function '%s'.", funcname);
  }

  for (int j = nouts - 1; j > 0; --j)
  {
    if (!lua_isnumber (L, -1))
    {
      SC_ABORTF ("Value is not a number: %s.", lua_tostring (L, -1));
    }

    fx[j] = lua_tonumber (L, -1);
    lua_pop (L, 1);
  }
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
