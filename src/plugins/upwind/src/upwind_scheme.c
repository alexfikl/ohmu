// SPDX-FileCopyrightText: 2014-2015 EM2C (Ecole Centrale Paris) - UPR288 CNRS
// SPDX-FileCopyrightText: 2014-2015 Maison de la Simulation (CEA/CNRS/INRIA/Univ. Paris Sud/UVSQ) - USR3441 CNRS
//
// SPDX-License-Identifier: CECILL-2.0

#include <ohmu_patch.h>

#include "upwind.h"
#include "upwind_bits.h"

/**
 * \brief Return the velocity in a specific direction with regards to a face.
 *
 * This function will returns u_x, u_y or u_z depending on the face that we
 * require. The idea is that, since we are on a cartesian grid, this will
 * just need to be multiplied by +1 or -1 to get the dot product
 * (u_jk, n_jk) for a face.
 */
static double
upwind_face_velocity (upwind_data_t cell, int8_t face)
{
  return cell.velocity[face / 2];
}

/**
 * \brief Compute a refinement indicator for the scalar advection.
 *
 * \param [in] c0 The value in a cell.
 * \param [in] c1 The value in a neighboring cell.
 *
 * \return A value between [0, 1] that is the indicator. For 1, it should
 * refine because the gradient is big.
 */
static double
upwind_indicator_gradient (double c0, double c1)
{
  double cmax = SC_MAX (fabs (c0), fabs (c1));

  if (cmax > 0.1)
  {
    return 1;
  }

  if (cmax < 0.03)
  {
    return 0;
  }

  cmax = fabs (c1 - c0) / cmax;
  return SC_MAX (SC_MIN (cmax, 1.0), 0.0);
}

/**
 * \brief Update the value in a cell with all the face fluxes.
 *
 * This function update the cell value using the 4 (or 6) neighbors and
 * the choice of flux that is the upwind flux. The upwind flux is given by:
 *
 *    flux = | cell value           if face velocity > 0
 *           | neighbor value       else
 *
 * The face velocity is actually (u_jk, n_jk) where u_jk is the mean value
 * of the velocity field on that edge and n_jk is the exterior normal to
 * the current cell.
 *
 * The full update formula is:
 *
 *    c_j^{n + 1} = c_j^n + dt / |T| sum h * (u_jk, n_jk) * (c_j^n - c_jk)
 *
 * where |T| is the area of the cell, h = dx = dy = dz is the length
 * of face (volume of a cell, respectively area of a face in 3D) and c_jk
 * is the flux defined above.
 *
 * This is formula is general (works for structured and unstructured meshes)
 * and can probably be simplified in our cartesian context.
 *
 * \param [in] data     The quadrant patch.
 * \param [in] cellid   The id of the current cell.
 * \param [in] size     The size of the patch with ghost cells.
 * \param [in] dt       The time step.
 * \param [in] dx       The size of the cell.
 */
static void
upwind_update_cell (upwind_data_t *cells, uint32_t cellid, uint32_t size,
                    double dt, double h)
{
  uint32_t nid[P4EST_FACES] = { cellid - 1,
                                cellid + 1,
                                cellid - size,
                                cellid + size
#ifdef P4_TO_P8
                                ,
                                cellid - size * size,
                                cellid + size * size
#endif
  };

  double c[2] = { 0, 0 };
  double velocity[2] = { 0, 0 };
  double n = 0;
  double face_velocity = 0;
  double flux = 0;

  // loop over all the neighbors of the cell
  for (int8_t face = 0; face < P4EST_FACES; ++face)
  {
    // get data in both cell
    c[0] = cells[cellid].c;
    c[1] = cells[nid[face]].c;

    // get the exterior normal
    n = 2 * (face % 2 == 1) - 1;

    // get the ux, uy or uz velocity, depending on the face
    velocity[0] = upwind_face_velocity (cells[cellid], face);
    velocity[1] = upwind_face_velocity (cells[nid[face]], face);

    // compute the face velocity
    face_velocity = n * (velocity[0] + velocity[1]) / 2.0;

    // compute the flux using the upwind choice
    flux = (face_velocity > 0 ? c[0] : c[1]);

    // update the cell
    cells[cellid].cnext += face_velocity * dt * (c[0] - flux) / h;
  }
}

/**
 * \brief Compute the time step inside a cell.
 *
 * Inside a cell, the allowed time step is given by:
 *    dt = h / sum ( u_jk, n_jk )
 * The sum is just over the negative terms, that is to say, only over those
 * faces where there current cell is the downwind cell.
 *
 * \param [in] data     The patch inside a quadrant.
 * \param [in] cellid   The id of the cell we're going to compute the time
 *                      step in.
 * \param [in] size     The size of the patch with the ghost cells.
 * \param [in] h        The real size of the cell.
 *
 * \return The time step.
 */
static double
upwind_cell_time_step (upwind_data_t *cells, uint32_t cellid, uint32_t size,
                       double h)
{
  uint32_t nid[P4EST_FACES] = { cellid - 1,
                                cellid + 1,
                                cellid - size,
                                cellid + size
#ifdef P4_TO_P8
                                ,
                                cellid - size * size,
                                cellid + size * size
#endif
  };

  double velocity[2] = { 0, 0 };
  double n = 0;
  double face_velocity = 0;
  double sum = 0;

  // loop over all the neighbors of the cell
  for (int8_t face = 0; face < P4EST_FACES; ++face)
  {
    // get the ux, uy or uz velocity, depending on the face
    velocity[0] = upwind_face_velocity (cells[cellid], face);
    velocity[1]
        = upwind_face_velocity (cells[nid[face]], p4est_face_dual[face]);

    // get the exterior normal
    n = 2 * (face % 2 == 1) - 1;

    // compute the face velocity
    face_velocity = n * (velocity[0] + velocity[1]) / 2.0;

    // sum the velocities on downwind edges
    sum += SC_MIN (face_velocity, 0);
  }

  if (!OHMU_FUZZY_NULL (sum))
  {
    return -h / sum;
  }

  // big value because we're taking the min
  return 42.0;
}

double
upwind_cfl (ohmu_solver_t *solver, p4est_quadrant_t *quad)
{
  upwind_data_t         *cells = upwind_quadrant_data (quad);

  ohmu_solver_settings_t settings = solver->settings;
  uint32_t               start = settings.patch_ghosts;
  uint32_t               end = settings.patch_size + settings.patch_ghosts;
  uint32_t               size = start + end;
  uint32_t               cellid = 0;
  uint32_t               xi = 0, yi = 0, zi = 0;

  double                 dt = 0.0;
  double                 dt_min = 2.0;
  double                 h = ohmu_quadrant_length (quad) / settings.patch_size;

#ifdef P4_TO_P8
  for (zi = start; zi < end; ++zi)
  {
#endif
    for (yi = start; yi < end; ++yi)
    {
      for (xi = start; xi < end; ++xi)
      {
        cellid = size * size * zi + size * yi + xi;

        // compute the min time step over all the patch cells
        dt = upwind_cell_time_step (cells, cellid, size, h);
        dt_min = SC_MIN (dt_min, dt);
      }
    }
#ifdef P4_TO_P8
  }
#endif

  return dt_min;
}

void
upwind_update (ohmu_solver_t *solver, p4est_quadrant_t *quad,
               p4est_topidx_t treeid, p4est_locidx_t quadid, double dt)
{
  UNUSED (treeid);
  UNUSED (quadid);

  ohmu_solver_settings_t settings = solver->settings;
  upwind_data_t         *cells = upwind_quadrant_data (quad);
  double                 h = ohmu_quadrant_length (quad) / settings.patch_size;

  uint32_t               start = settings.patch_ghosts;
  uint32_t               end = settings.patch_size + settings.patch_ghosts;
  uint32_t               size = start + end;
  uint32_t               cellid = 0;
  uint32_t               xi = 0, yi = 0, zi = 0;

#ifdef P4_TO_P8
  for (zi = start; zi < end; ++zi)
  {
#endif
    for (yi = start; yi < end; ++yi)
    {
      for (xi = start; xi < end; ++xi)
      {
        cellid = size * size * zi + size * yi + xi;

        upwind_update_cell (cells, cellid, size, dt, h);
      }
    }
#ifdef P4_TO_P8
  }
#endif
}

double
upwind_refine_indicator (ohmu_solver_t *solver, p4est_quadrant_t *q)
{
  upwind_data_t         *cells = upwind_quadrant_data (q);

  ohmu_solver_settings_t settings = solver->settings;
  uint32_t               patch_size = settings.patch_size;
  uint32_t               patch_ghosts = settings.patch_ghosts;
  ohmu_patch_iterator_t  it = ohmu_iterator_new (patch_size, patch_ghosts);

  double                 eps = 0.0;
  double                 eps_max = 0.0;
  uint32_t               nid[P4EST_FACES];

  ohmu_quadrant_print (SC_LP_INFO, q);
  upwind_patch_print (cells, patch_size, patch_ghosts);

  ohmu_iterator_reset (&it);
  while (it.current < it.last)
  {
    ohmu_iterator_neighbors (&it, nid);

    for (int face = 0; face < P4EST_FACES; ++face)
    {
      eps = upwind_indicator_gradient (cells[it.current].c,
                                       cells[nid[face]].c);
      eps_max = SC_MAX (eps_max, eps);
    }
    ohmu_iterator_next (&it);
  }

  ohmu_quadrant_print (SC_LP_INFO, q);
  printf ("eps_max = %g\n", eps_max);

  return eps_max;
}

void
upwind_replace_fn (p4est_t *p4est, int num_outgoing, void *out[],
                   int num_incoming, void *in[])
{
  UNUSED (p4est);

  upwind_data_t **cellout = (upwind_data_t **)out;
  upwind_data_t **cellin = (upwind_data_t **)in;

  if (num_outgoing == 1)
  { // refining
    for (int i = 0; i < num_incoming; ++i)
    {
      memcpy (cellin[i], cellout[0], sizeof (upwind_data_t));
    }
  }
  else
  { // coarsening
    upwind_data_t mean;
    memset (&mean, 0, sizeof (upwind_data_t));

    // compute a mean of the 4 / 8 outgoing cells
    for (int i = 0; i < num_outgoing; ++i)
    {
      mean.c += cellout[i]->c / num_outgoing;
      mean.cnext = mean.c;
      mean.velocity[0] += cellout[i]->velocity[0] / num_outgoing;
      mean.velocity[1] += cellout[i]->velocity[1] / num_outgoing;
#ifdef P4_TO_P8
      mean.velocity[2] += cellout[i]->velocity[2] / num_outgoing;
#endif
    }

    // copy the mean into the incoming cell
    memcpy (cellin[0], &mean, sizeof (upwind_data_t));
  }
}

void
upwind_replace_refined (p4est_t *p4est, p4est_topidx_t treeid,
                        p4est_quadrant_t *quadout,
                        p4est_quadrant_t *quadin[P4EST_CHILDREN])
{
  UNUSED (treeid);

  ohmu_patch_replace (p4est, 1, &quadout, P4EST_CHILDREN, quadin,
                      upwind_replace_fn);
}

void
upwind_replace_coarsened (p4est_t *p4est, p4est_topidx_t treeid,
                          p4est_quadrant_t *quadout[P4EST_CHILDREN],
                          p4est_quadrant_t *quadin)
{
  UNUSED (treeid);

  ohmu_patch_replace (p4est, P4EST_CHILDREN, quadout, 1, &quadin,
                      upwind_replace_fn);
}

int
upwind_should_finish (ohmu_solver_t *solver)
{
  UNUSED (solver);

  return 1;
}

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
